#!/bin/python

"""
generate_images.py
jlazear
1/22/15

Generates images used in the docs files.

Example:

$ python generate_images.py
"""
__version__ = 20150122
__releasestatus__ = 'beta'


import os
import sys
import argparse

import wget
import numpy as np
from scipy.optimize import brentq
import healpy as hp
import matplotlib.pyplot as plt

imgpath = os.path.dirname(os.path.abspath(__file__)) + '/'


# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("--regenerate",
                    help="force the regeneration of all figures",
                    action="store_true")
args = parser.parse_args()
force = bool(args.regenerate)

strtemplate = "Saving {0} to {1}\r\n"
strexists = "File {0} already exists."

# -------------------------------------
# ---------- Generate images ----------
# -------------------------------------

# inductances.png
fname = imgpath + 'inductances.png'
if os.path.isfile(fname) and not force:
    print strexists.format(fname)
else:
    f_Ny = 10.e3
    tau_Ny = 1./(2*np.pi*f_Ny)
    f_VPM = 3.
    tau_VPM = 1./(2*np.pi*f_VPM)
    # print "tau_Ny =", tau_Ny
    # print "tau_VPM =", tau_VPM

    f_VPM = 3.  # Hz
    w_VPM = 2*np.pi*f_VPM  # rad/s
    tau_VPM = 1/w_VPM  # s/rad

    F = 10.

    R_L = 2.e-3  # 2 mOhm
    R0 = 8.e-3  # 8 mOhm
    LI = 25.  # Loop gain
    beta_I = 0.3  # Dimensionless current sensitivity
    tau = np.linspace(0.002, 0.02)  # seconds, lower and upper bounds (from Benford)

    tau_I = tau/(1 - LI)

    Lmin = ((R_L + R0*(1 + beta_I)) - tau_Ny/tau * (LI*(R0-R_L)+ R_L + R0*1 + beta_I))/(1./tau_Ny - 1/tau_I)

    Lcritminus = (LI * (3 + beta_I - R_L/R0) + (1 + beta_I+ R_L/R0)
                  - 2*np.sqrt(LI*(2 + beta_I)*(LI*(1 - R_L/R0)
                                               + (1 + beta_I + R_L/R0)))) * (R0*tau)/((LI - 1)**2)

    fig, ax = plt.subplots()
    ax.plot(tau*1.e3, Lmin*1.e9, label=r'$L_\mathrm{min}$')
    ax.plot(tau*1.e3, Lcritminus*1.e9, label=r'$L_{\mathrm{crit}-}$')
    ax.axhline(400., ls='--')
    ax.set_xlabel(r'$\tau = C/G$ (ms)', fontsize=24)
    ax.set_ylabel(r'Inductance (nH)', fontsize=24)
    ax.legend(loc='upper left', fontsize=20)

    print strtemplate.format('TES Inductances', fname)
    fig.savefig(fname)
    fig.clf()

# filter data files
fnames = [imgpath + 'no_alias.txt', imgpath + 'alias_with_ubc.txt',
          imgpath + 'alias_wo_ubc.txt', imgpath + 'fir_transfer.txt']
allthere = all(map(os.path.isfile, fnames))
if allthere and not force:
    for fname in fnames:
        print strexists.format(fname)
else:
    # Define UBC Amplifier response
    poles_ubcamp = np.array([9.7, 9.7, 15.2, 15.2, 7.2])*1.e6
    gain_ubcamp = 203.
    hs_ubcamp = np.vectorize(lambda f: 203./np.sqrt(np.prod([(1. + (f/p)**2)
                                                             for p in poles_ubcamp])))

    # Define row dwell FIR
    f_MCE = 50.e6
    sample_num = 10
    sample_dly = 90
    row_len = sample_num + sample_dly
    num_row = 41
    t_s = sample_num/f_MCE

    h_fir = lambda f: np.abs(np.sinc(t_s*f))

    # fs = np.logspace(3, np.log10(25.e6), 200)
    fs = np.linspace(0, 25.e6, 200)
    fs[0] = 1.e3
    hs_fir = h_fir(fs)

    print strtemplate.format('FIR Transfer Function', 'fir_transfer.txt')
    np.savetxt(imgpath + 'fir_transfer.txt', np.array((fs/1.e6, hs_fir)).T)

    # plt.plot(fs, hs_fir, 'o')
    # plt.xscale('log')
    # plt.xlim(xmax=25.e6)

    ## Define aliased FIR ##
    f_max = f_MCE/(2*num_row*row_len)
    foffset = 2*f_max
    fs = np.linspace(-f_max, f_max, 200)

    # Find how many instances we need to count for aliasing
    bfunc = lambda f, c: hs_ubcamp(f)**2/hs_ubcamp(0)**2 - c
    fc = brentq(bfunc, 1.e6, 100.e6, args=(.01))
    n_alias = int(fc/f_max)

    # Compute aliased FIR power
    aliased = np.array([hs_ubcamp(fs + n*foffset)**2/hs_ubcamp(0)**2 * h_fir(fs + n*foffset)**2
                        for n in range(-n_alias, n_alias)])
    aliased = np.sqrt(np.sum(aliased, axis=0))
    aliased2 = np.array([h_fir(fs + n*foffset)**2 for n in range(-n_alias*2, n_alias*2)])
    aliased2 = np.sqrt(np.sum(aliased2, axis=0))
    aliased3 = h_fir(fs)**2
    aliased3 = np.sqrt(aliased3)

    # Save data files
    print strtemplate.format('Aliased FIR Transfer Function (w/ UBC Amplifier Filter)', 'alias_with_ubc.txt')
    np.savetxt(imgpath + 'alias_with_ubc.txt', np.array((fs/1.e3, aliased)).T)
    print strtemplate.format('Aliased FIR Transfer Function (w/o UBC Amplifier Filter)', 'alias_wo_ubc.txt')
    np.savetxt(imgpath + 'alias_wo_ubc.txt', np.array((fs/1.e3, aliased2)).T)
    print strtemplate.format('Un-aliased FIR Transfer Function', 'no_alias.txt')
    np.savetxt(imgpath + 'no_alias.txt', np.array((fs/1.e3, aliased3)).T)

    # plt.plot(fs, aliased, label='UBC Filter')
    # plt.plot(fs, aliased2, label='No Filter')
    # plt.plot(fs, aliased3, label='No Aliasing')
    # plt.xlim(xmin=-f_max, xmax=f_max)
    # plt.legend()
    # plt.ylim(ymin=0, ymax=1.1*np.mean(aliased2))


# mce_feedback_transfer.txt
fname = imgpath + 'mce_feedback_transfer.txt'
if os.path.isfile(fname) and not force:
    print strexists.format(fname)
else:
    def H(ws, kappa):
        return 1./np.sqrt(1. + (ws/kappa)**2 - 2*(ws/kappa)*np.sin(2*np.pi*ws))

    # ws = np.linspace(0, 0.5, 100)
    ws = np.logspace(-3, np.log10(0.5), 100)
    Hs = lambda kappa: H(ws, kappa)

    kappas = [.01, .03, .05, .0781, 0.2, .5, 1]
    a = np.array([ws] + [Hs(kappa) for kappa in kappas])
    print strtemplate.format('MCE Feedback Loop Transfer Function', fname)
    np.savetxt(fname, a.T)

# mce_feedback_transfer_max.txt
fname = imgpath + 'mce_feedback_transfer_max.txt'
if os.path.isfile(fname) and not force:
    print strexists.format(fname)
else:
    def H(ws, kappa):
        return 1./np.sqrt(1. + (ws/kappa)**2 - 2*(ws/kappa)*np.sin(2*np.pi*ws))

    ws = np.linspace(0, 0.5, 1000)
    Hs = lambda kappa: H(ws, kappa)
    metric = lambda kappa: np.max(H(ws, kappa))

    kappas = np.linspace(0, 1, 500)[1:]
    metrics = [metric(kappa) for kappa in kappas]
    a = np.array([kappas, metrics])
    print strtemplate.format('MCE Feedback Loop Transfer Function Maxima', fname)
    np.savetxt(fname, a.T)