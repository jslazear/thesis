% Single Pixel Characterization
% - TES physics
% - SQUID readout circuit
% - SHINY testbed description
% - Single pixel test apparatus
% - Expected I-V curves, sources of noise in power spectrum
% - Single pixel performance

\chapter{Superconducting Quantum Interference Devices (SQUIDs)}
\label{sec:squids}

The detector multiplexer and readout both rely heavily on Superconducting
Quantum Interference Devices (SQUIDs). We provide an overview of the relevant
pieces of the physics of SQUIDs so that we may better understand the mux and
readout.

\section{Josephson Junctions}
\label{sec:josephson_junctions}

The foundation of SQUIDs is the Josephson Junction. A Josephson Junction is a
sandwich of superconductor-insulator-superconductor\footnote{The middle part
does not strictly need to be an insulator. It may be any material that
inhibits the coherence of the superconducting phase parameter. A normal metal
is most commonly used in SQUIDs, though a weakened superconductor (e.g. by
making the superconductor thinner, or via the proximity effect of a normal
metal) works just as well.}. Our analysis follows that of
Feynman\cite{feynman_feynman_1971}. The macroscopic wavefunction of each of
the superconductors may be written
%
\begin{subequations}\label{eq:wavefunctions}
\begin{align}
	\psi_1 & = \sqrt{\rho_1} e^{i \phi_1} \\
	\psi_2 & = \sqrt{\rho_2} e^{i \phi_2}
\end{align}
\end{subequations}

The (real-valued) coefficients $\rho_i$ are the charge density of the Cooper
pairs (units of charge/volume), with charge $q = 2e$, in superconductor $i$,
and the phases $\phi_i$ are quantum mechanical parameters describing the
coherence of the superconductivity in superconductor $i$. For a strongly
coupled superconductor, the phase parameter is continuous and varies with the
magnetic potential\cite{tinkham_introduction_2004,
sadleir_superconducting_2011}. We will see shortly that the insulator causes a
discontinuity in the phase parameter across the barrier between
superconductors 1 and 2.

We apply a voltage $V$ across the junction (one lead connected to
superconductor 1 and the other lead connected to superconductor 2). Then the
system may be described by a system of coupled Schrodinger Equations,
%
\begin{subequations}
\begin{align}
	i\hbar \pderiv{\psi_1}{t} & = eV\psi_1 + K \psi_2 \\
	i\hbar \pderiv{\psi_2}{t} & = -eV\psi_2 + K \psi_1
\end{align}
\end{subequations}
%
where we have defined the zero energy level to be half of the energy shift as
a Cooper pair crosses the insulator, $\Delta E = 2eV$, and implicitly defined
both superconductors as having identical properties. Inserting
Eqs.~\eqref{eq:wavefunctions} into this system gives us the equations
%
\begin{subequations}
\begin{align}
	\dot{\rho_1} = -\dot{\rho_2} & = \frac{2 K}{\hbar} \sqrt{\rho_1 \rho_2} \sin \phi \\
	\dot{\phi_1} & = -\frac{e V}{\hbar} - \frac{K}{\hbar} \sqrt{\frac{\rho_2}{\rho_1}} \cos\phi \\
	\dot{\phi_2} & = \frac{e V}{\hbar} - \frac{K}{\hbar} \sqrt{\frac{\rho_1}{\rho_2}} \cos\phi
\end{align}
\end{subequations}
%
where we have defined $\phi = \phi_2 - \phi_1$. We note that $\dot{\rho_1}$
and $-\dot{\rho_2}$ are the rates at which Cooper pairs enter superconductor 1
and leave superconductor 2, respectively. Since the two ends are connected by
a wire (i.e. whatever circuit is generating $V$), electrons that leave
superconductor 2 will eventually find their way back to superconductor 1.
Thus, charge is conserved. Furthermore, since the superconductors have
identical properties, the Cooper pair density should be the same in both, so
$\rho_1 = \rho_2 = \rho_0$. The interpretation of $\dot{\rho_i}$ is then the
amount of charge passing through some region in a period of time, forming a
current density
%
\begin{align}
	J & = \dot{\rho} = \frac{2K}{\hbar} \rho_0 \sin\phi \notag \\
	J & = J_0 \sin\phi
\end{align}
%
Note that this is the amount of charge passing through the volume, not the
change in the amount of charge contained in the volume (which is essentially
constant). The way to convert from $J$ to proper current $I$ in Amps depends
on the type and geometry of the junction. The conversion does not depend on
any of the values comprising $J$, so there is simply a constant of
proportionality. We may then write the current,
%
\begin{equation}
	I_c = I_0 \sin\phi \label{eq:first_josephson}
\end{equation}
%
where the maximum critical current $I_0$ is simply some value that is
typically measured rather than computed. Eq.~\eqref{eq:first_josephson} is the
first Josephson Equation. We can interpret this equation by noting that when
$V=0$, the phase difference $\phi$ is constant. Since the allowed current is
non-zero but the voltage is 0, this current is a supercurrent, which is
allowed up to $I_c$. Thus we identify $I_c$ as the critical supercurrent
allowed across the junction, which is attenuated by the two superconductors
interfering with each other.

Next we note that $\dot{\phi} = \dot{\phi_2} - \dot{\phi_1}$, which combined
with our equations for $\dot{\phi_i}$ gives us the second Josephson equation,
%
\begin{equation}
	V = \frac{\hbar}{2e} \dot{\phi}. \label{eq:second_josephson}
\end{equation}
%
The second Josephson Equation shows that a constant voltage will cause the
phase difference to oscillate, which in turn will cause the critical current
to oscillate.

We can model the Josephson Junction as a non-linear inductor by applying the
Josephson equations Eqs.~\eqref{eq:first_josephson} and
\eqref{eq:second_josephson} to the voltage drop across an inductor
%
\begin{align}
	V & = L \D{I}{t} \notag \\
	\implies L & = \frac{\hbar}{2e I_0} \frac{1}{\cos\phi} \equiv \frac{L_J}{\cos\phi} \\
	\text{where}\quad L_J & \equiv \frac{\hbar}{2e I_0} = \frac{\Phi_0}{2\pi I_0} = \frac{\left( \unit[0.3]{nH\cdot\mu A} \right)}{I_0}.
\end{align}
%
Since the superconducting phase parameter varies with the magnetic potential,
it can be shown\cite{tinkham_introduction_2004, sadleir_superconducting_2011}
that the phase difference across the Josephson Junction is
%
\begin{equation}
	\phi = \frac{2\pi\Phi}{\Phi_0} \label{eq:phidiff}
\end{equation}
%
where $\Phi$ is the flux enclosed by the junction. Since $\phi$ is periodic,
changes in $\Phi$ of $\Phi_0$ do not result in a change in the behavior of the
system.

As a final note, Josephson Junctions are inherently quantum devices that rely
on the interference between the superconducting phase parameters between
superconductors 1 and 2. Thermal fluctuations can wash out the interference
pattern and destroy the quantum properties of the Josephson Junction,
including all supercurrent effects. If the thermal fluctuations result in a
variation in the flux of $\delta \Phi \gtrsim \nicefrac{\Phi_0}{2}$, then the
phase difference will be essentially randomized. We may estimate the scale of
the thermal fluctuations with the Equipartition Theorem and use it to
constrain the inductance,
%
\begin{align}
	\frac{1}{2} \frac{(\delta \Phi)^2}{L} & = \frac{1}{2} kT \notag \\
	\frac{1}{8} \frac{\Phi_0^2}{L} & \gtrsim \frac{1}{2}kT \notag \\
	L & \lesssim \frac{\Phi_0^2}{4kT} = \frac{(\unit[80]{nH\cdot K})}{T} \label{eq:L_cond}
\end{align}

\section{DC SQUIDs}
\label{sec:dc_squids}

A DC SQUID is a loop of superconducting metal with Josephson Junctions at
opposite ends (Figure~\ref{fig:squid}). The superconductors on the same side
of both Josephson Junctions is shared, so there are only two total
superconductors. Leads on each side of the Josephson Junctions apply voltage
and transport current to the SQUID. The total inductance of the SQUID is the
sum of both the Josephson Junction inductance $L_J$ and the loop inductance
$L$. We will first examine the behavior of our SQUID with no voltage drop,
then see what happens in the presence of a non-zero voltage. Much of this
discussion follows that of Tinkham\cite{tinkham_introduction_2004}.

\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{squid}
\end{center}
\caption{A DC SQUID is a superconducting loop with Josephson Junctions at each
         end. The superconducting loop can intercept magnetic flux $\Phi$,
         which changes the behavior of the SQUID.}
\label{fig:squid}
\end{figure}

Since there are Josephson Junctions on each arm of the SQUID, it may admit
current even in the absence of a voltage. Each Josephson Junction has a phase
difference, which we write as $\phi_A$ and $\phi_B$ for junctions A and B,
respectively. We redefine $\phi \equiv \phi_B - \phi_A$. We may write the
current through the SQUID using the first Josephson Equation,
%
\begin{align*}
	I & = I_0\left( \sin\phi_A + \sin\phi_B \right) \\
	I & = 2 I_0 \sin \left( \frac{\phi_A + \phi_B}{2} \right) \cos \left( \frac{\phi_B - \phi_A}{2} \right) \\
	I & = 2 I_0 \sin \left( \frac{\phi_A + \phi_B}{2} \right) \cos \left( \frac{\pi \Phi}{\Phi_0} \right)
\end{align*}
%
where we have utilized the result $\phi = 2\pi \Phi/\Phi_0$, which is the
2-junction form of Eq.~\eqref{eq:phidiff}. The current is maximized when
$\phi_A + \phi_B = \pi \implies \phi_A = \pi/2 - \pi \Phi/\Phi_0$ and $\phi_B
= \pi/2 + \pi \Phi/\Phi_0$, with maximum value (Fig~\ref{fig:squid_1})
%
\begin{equation}
	I_c = 2I_0 \left| \cos \left( \frac{\pi \Phi}{\Phi_0} \right) \right|,
\end{equation}
%
which describes the maximum amount of supercurrent that the SQUID will admit
without inducing a voltage drop.

\ssp
\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{squid_1}
\end{center}
\caption{The maximum critical current $I_c$ of a DC SQUID with no voltage
         across it depends on the magnetic flux $\Phi$ intercepted by the
         loop, and forms a double-slit interference pattern with spacing
         $\Phi/\Phi_0$. (\emph{Solid}) A symmetric SQUID with negligible
         screening currents ($\beta_L \ll 1$) has a maximum value of twice the
         individual Josephson Junction critical current $I_0$ and a minimum of
         0. (\emph{Dashed}) When screening currents are significant ($\beta_L
         \gtrsim 1$), the troughs are not as deep and reach a minimum at
         $I_\mathrm{min}$. In the strong screening limit ($\beta_L \gg 1$),
         the peak-to-trough distance is $\Delta I_c \sim \Phi_0/L$,
         independent of $I_0$.}
\label{fig:squid_1}
\end{figure}
\dsp

If the applied current $I$ exceeds this maximum value $I_c(\Phi)$, then the
excess current will be admitted through the impedance $R = R_\mathrm{JJ} ||
R_\mathrm{JJ} = R_\mathrm{JJ}/2$ of the SQUID, which is half of the impedance
of a single Josephson Junction $R_\mathrm{JJ}$. Note, however, that once a
voltage is applied to the SQUID, the phase differences across the Josephson
Junctions $\phi_A$ and $\phi_B$ will evolve, so we cannot simply use $I_c$
(since $I_c$ relies on a particular choice of $\phi_A + \phi_B$) and must
instead use the more generic Josephson currents, resulting in
%
\begin{align}
	I & = I_0\left( \sin \phi_A + \sin \phi_B \right) + \frac{V}{R} \notag \\
	V & = R \left[ I - 2I_0 \cos\left( \frac{\pi \Phi}{\Phi_0} \right) \sin \bar{\phi} \right], \label{eq:squid_V}
\end{align}
%
where we have defined $\bar{\phi} \equiv \frac{1}{2}\left( \phi_A + \phi_B
\right)$ to be the average phase difference. The effects of Junction
capacitance could also be included in Eq.~\eqref{eq:squid_V} to make what is
called the RCSJ model\cite{clarke_squid_2004}, but the capacitances of the
point junctions we use are negligible, so we do not include it here. Since the
voltage varies rapidly in time (assuming it is not $\Phi = \Phi_0/2$), but we
measure the voltage only slowly, we are interested in the time-averaged
voltage $\Avg{V}$. Finally, we note that $\dot{\bar{\phi}} =
\frac{1}{2}\left( \dot{\phi_A} + \dot{\phi_B} \right) = 2eV/\hbar$, from the
second Josephson Equation. So,
%
\begin{align}
	\Avg{V} & = \frac{\hbar}{2e} \Avg{\dot{\bar{\phi}}} = \frac{\hbar}{2e} \frac{\oint \dot{\bar{\phi}}\,\dd t}{\oint \dd t} \notag \\
	& = \frac{\hbar}{2e} \frac{\int_0^{2\pi} \dd \bar{\phi}}{\int_0^{2\pi} \frac{1}{ \left( \D{\bar{\phi}}{t} \right)}\,\dd\bar{\phi}} = \frac{2\pi}{\int_0^{2\pi} \frac{\dd \bar{\phi}}{V(\bar{\phi})}} \label{eq:squid_vbar_general} \\
	\Avg{V} & = 2\pi \left( \int_0^{2\pi} \frac{\dd\bar{\phi}}{ R \left[ I - 2I_0 \cos\left( \frac{\pi \Phi}{\Phi_0} \right) \sin \bar{\phi} \right]} \right)^{-1}. \label{eq:squid_vbar}
\end{align}
%
Note that Eq.~\eqref{eq:squid_vbar_general} is generally true and can be used
to find (most likely numerically) the response of a generic SQUID, e.g. using
the RCSJ model, but Eq.~\eqref{eq:squid_vbar} is valid only for our simpler
case. It may be integrated to give
%
\begin{equation}
	\Avg{V} = \begin{cases}
	0 & \text{for } |I| \leq I_S\cos\left( \frac{\pi \Phi}{\Phi_0} \right) \\
	I_S R \sqrt{\left( \frac{I}{I_S} \right)^2 - \cos^2{\left( \frac{\pi \Phi}{\Phi_0} \right)}} & \text{for } |I| > I_S\cos\left( \frac{\pi \Phi}{\Phi_0} \right)
	\end{cases} \label{eq:vresponse}
\end{equation}
%
where $I_S \equiv 2I_0$ is the total maximum critical current of the SQUID,
which is twice that of an individual Josephson Junction. We note that it has a
maximum value when $\Phi = (n + \nicefrac{1}{2})\Phi_0$ and a minimum value
when $\Phi = n\Phi_0$. For $|I| < I_S \cos\left( \frac{\pi \Phi}{\Phi_0}
\right)$, the SQUID can admit the entire bias current as supercurrent, and so
the voltage is $\Avg{V} = 0$.
Figures~\ref{fig:squid_2},~\ref{fig:squid_3},~and~\ref{fig:squid_4} show the
$I$-$\Avg{V}$, $I$-$R_\mathrm{eff}$, and $\Avg{V}$-$\Phi$ curves,
respectively.

\ssp
\afterpage{\clearpage}
\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{squid_2}
\end{center}
\caption{The $I$-$\Avg{V}$ curve for a DC SQUID. The true voltage is
	     oscillating at a higher frequency than we measure, so we instead plot
	     the time-averaged value $\Avg{V}$. The uppermost curve corresponds to
	     $\Phi = (n+\nicefrac{1}{2})\Phi_0$, for which the SQUID critical
	     current $I_c$ has been fully suppressed. This curve is valid only for
	     the weak screening ($\beta_L \ll 1$) limit. The SQUID behaves as a
	     resistor with resistance $R=R_\mathrm{JJ} || R_\mathrm{JJ} =
	     R_\mathrm{JJ}/2$, where $R_\mathrm{JJ}$ is the impedance of an
	     individual Josephson Junction. The lowermost curve corresponds to
	     $\Phi = n\Phi_0$, for which the SQUID critical current is not
	     suppressed at all. The SQUID admits supercurrent up to its critical
	     current $I_S = 2I_0$, twice the critical current of an individual
	     Josephson Junction. Any excess current is shunted through the
	     impedance of the SQUID. (\emph{Dashed}) The dashed line shows the
	     effect of a screening current ($\beta_L \gtrsim 1$) on the maximum
	     voltage response, $\Phi = (n+\nicefrac{1}{2})\Phi_0$. As the
	     screening strength increases (increasing $\beta_L$), the line moves
	     more towards the minimum curve, but can get no closer than $\Delta I
	     \sim \Phi_0/L$ at $V=0$. The minimum response is unchanged.}
\label{fig:squid_2}
\end{figure}
\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{squid_3}
\end{center}
\caption{The $I$-$R_\mathrm{eff}$ curve for a DC SQUID. The uppermost curve
         corresponds to $\Phi = (n+\nicefrac{1}{2})\Phi_0$, for which the
         SQUID is totally resistive, valid only in the weak-screening
         ($\beta_L \ll 1$) limit. The lowermost curve corresponds to
         $\Phi = n\Phi_0$, for which the SQUID critical current is not
         suppressed at all. The SQUID admits supercurrent up to its critical
         current then passes the remaining current resistively.
         (\emph{Dashed}) The dashed line shows the effect of a screening
         current ($\beta_L \gtrsim 1$) on the maximum effective resistance. As
         the screening strength increases (increasing $\beta_L$), the line
         moves more towards the minimum curve, but can get no closer than
         $\Delta I \sim \Phi_0/L$ at $R_\mathrm{eff} = 0$.}
\label{fig:squid_3}
\end{figure}
\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{squid_4}
\end{center}
\caption{The $\Avg{V}$-$\Phi$ curve for a DC SQUID at a few representative
         bias current values $I$. (\emph{Solid}) The SQUID is partially on
         when $I < I_S$. For fluxes $\Phi$ near $n\Phi_0$, the critical
         current is large enough to admit the full bias current as
         supercurrent, but must shunt some current through the impedance for
         fluxes near $(n+\nicefrac{1}{2})\Phi_0$. (\emph{Dashed}) The SQUID
         has just fully turned on when $I = I_S$. It can admit the full bias
         current as supercurrent only when $\Phi = n\Phi_0$. The peak-to-peak
         amplitude is maximized here. (\emph{Dash-dotted}) The SQUID is fully
         on when $I > I_S$. It must always shunt current through its
         impedance. The peak-to-peak amplitude decreases as $I$ increases
         above $I_S$. Note that the qualitative behavior is not significantly
         different between the weak-screening and non-weak-screening cases.
         All that changes is the SQUID will not turn partially on until the
         bias current exceeds the minimum critical current,
         $I_\mathrm{min} < I < I_S$. The SQUID is fully off (and has no
         voltage response) when $I < I_\mathrm{min}$.}
\label{fig:squid_4}
\end{figure}
\dsp

When a flux $\Phi$ is applied to the SQUID loop, Lenz's Law dictates that a
screening current $I_L$ will be generated in the loop. The screening current
will generate a screening flux with magnitude $\Phi_L = L I_L$. This screening
flux prevents the total flux in the loop from reaching
$(n+\nicefrac{1}{2})\Phi_0$, so the critical current is never fully suppressed
(see Fig~\ref{fig:squid_1}. Note that once the applied flux exceeds
$\Phi_0/2$, it becomes more energetically favorable (in the sense that the
screening current required is smaller) to push the total flux towards
$\Phi_0$, so in that case the screening flux will switch signs. The screening
strength is parameterized by the screening parameter
%
\begin{equation}
	\beta_L \equiv 2\pi L I_0/\Phi_0 = L/L_J.
\end{equation}
%
For $\beta_L \gg 1$, the screening dominates and the total flux is always near
$n\Phi_0$. For $\beta_L \ll 1$, the screening is negligible. The screening
factor determines how large the minimum critical current $I_\mathrm{min}$ is,
below which the SQUID is fully off. The voltage response and effective
resistance are also affected by $\beta_L$ (see
Figs.~\ref{fig:squid_2}~and~\ref{fig:squid_3}). The $\Avg{V}$-$\Phi$ response
is largely unchanged except for the fully off state. Note that SQUIDs with
large loops will have a large inductance $L$ and consequently a large
screening parameter $\beta_L$. This is common for the coupling SQUIDs in the
detector readout. The flux-gated switches in the multiplexer must have
$\beta_L \ll 1$ so that there is a large change in effective resistance
between the on and off states. As a final note, the condition in
Eq.~\eqref{eq:L_cond} must also be satisfied for the SQUID loop with the SQUID
loop inductance $L$, in addition to for the Josephson Junctions themselves. If
the condition is not satisfied, the double-slit interference pattern will be
washed out.

\section{Voltage and Current Biases}
\label{sec:voltage_and_current_biases}

DC SQUIDs may be current biased or voltage biased\footnote{Or even weakly
biased with some arbitrary $I = f(V)$ curve, though we do not consider this
case}. We briefly examine these two cases and the SQUID response in each.

\noindent\textbf{Current Bias}

In the current biased case, $I = I_\mathrm{bias} = \text{constant}$, and we
are interested in the voltage $\Abs{V}$ response to changing flux $\Phi$. When
constructing the equations, we were implicitly working in this regime, and we
simply reproduce the response derived above (Eq.~\eqref{eq:vresponse}) and
plotted in Fig~\ref{fig:squid_4}.
%
\begin{equation}
	\Avg{V} = \begin{cases}
	0 & \text{for } \cos \left( \frac{\pi \Phi}{\Phi_0} \right) \geq \frac{|I_\mathrm{bias}|}{I_S} \\
	I_S R \sqrt{\left( \frac{I_\mathrm{bias}}{I_S} \right)^2 - \cos^2{\left( \frac{\pi \Phi}{\Phi_0} \right)}} & \text{for } \cos \left( \frac{\pi \Phi}{\Phi_0} \right) \leq \frac{|I_\mathrm{bias}|}{I_S}
	\end{cases}
\end{equation}

\noindent\textbf{Voltage Bias}

In the voltage biased case, $\Avg{V} = V_\mathrm{bias} = \text{constant}$, and
we are interested in the current $I$ response to changing flux $\Phi$. We
simply invert Eq.~\eqref{eq:vresponse} to get the response and plot it in
Fig~\ref{fig:squid_5}. We also plot the effective resistance $R_\mathrm{eff}$
in Fig~\ref{fig:squid_6}.
%
\begin{equation}
	I = I_S \sqrt{ \left( \frac{V_\mathrm{bias}}{I_S R} \right)^2 + \cos^2 \left( \frac{\pi \Phi}{\Phi_0} \right)}
\end{equation}

\ssp
\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{squid_5}
\end{center}
\caption{The $I$-$\Phi$ curve for a DC SQUID at a few representative
         bias voltage values $\Avg{V}$. (\emph{Solid}) The $V_\mathrm{bias}=0$
         case. This is simply the zero-voltage critical current curve shown in
         Fig~\ref{fig:squid_1}. Note that as before, this curve is valid for
         the weak-screening limit ($\beta_L \ll 1$). In other screening
         strength regimes ($\beta_L \gtrsim 1$), the minimum current does not
         reach zero. (\emph{Dashed}) The $V_\mathrm{bias}=I_S R$ case. The
         peak-to-peak amplitude decreases with increasing $V_\mathrm{bias}$,
         though there is no particular critical value where interesting
         behavior occurs. For non-weak-screening regimes, the troughs are
         not as low, resulting in a smaller peak-to-peak amplitude.
         (\emph{Dash-dotted}) The $V_\mathrm{bias}=2 I_S R$ case.}
\label{fig:squid_5}
\end{figure}
\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{squid_6}
\end{center}
\caption{The $\Avg{V}$-$R_\mathrm{eff}$ curve for a DC SQUID. The uppermost
	     curve corresponds to $\Phi = (n+\nicefrac{1}{2})\Phi_0$, for which
	     the SQUID is totally resistive, valid only in the weak-screening
	     ($\beta_L \ll 1$) limit. The lowermost curve corresponds to
	     $\Phi = n\Phi_0$, for which the critical current is not suppressed
	     and a portion of the current is passed through the SQUID as
	     supercurrent, thus lowering the effective resistance. (\emph{Dashed})
	     The dashed line shows the effect of a screening current
	     ($\beta_L \gtrsim 1$) on the maximum effective resistance. As the
	     screening strength increases (increasing $\beta_L$), the top curve
	     moves toward the minimum curve.}
\label{fig:squid_6}
\end{figure}
\dsp

Note that the current can go to zero only in the weak screening ($\beta_L \ll
0$) regime with zero bias voltage ($V_\mathrm{bias} = 0$). The peak-to-peak
amplitude of the response decreases with increasing bias voltage and with
increasing screening strength $\beta_L$.

\section{Flux-gated Switches}
\label{sec:flux_gated_switches}

We observe in Figs.~\ref{fig:squid_3}~and~\ref{fig:squid_6} that the
resistance of a DC SQUID can be controlled effectively by changing the flux
applied to the loop. This is the basis of a switch: by changing the impedance
of one path from low to high diverts current to alternative pathways. The
basic circuit for using a SQUID as a flux-gated switch is shown in
Fig.~\ref{fig:basic_switch}.

\ssp
\begin{figure}[ht]
\begin{center}
\subimport{imgs/}{basic_switch}
\end{center}
\caption{A flux-gated switch. Applying current to RS actuates the resistance
        of SN, effectively shorting or opening the SN arm. This turns off or
        on the load.}
\label{fig:basic_switch}
\end{figure}
\dsp

The bias may be either a voltage or current bias. The current bias case is
simpler to understand, so we cover it first. The voltage bias case is similar
in principle, but is less obviously extensible.

Suppose the bias is a current bias $I_\mathrm{bias} \lesssim I_S$ compared to
the switch SQUID's (SN) max critical current $I_S$. We may apply flux through
the coil coupling into the switch SQUID. If the applied flux is near $\Phi =
n\Phi_0$ (the OFF or CLOSED state), then the critical current in SN is not
suppressed, and SN can pass the full bias current with no resistance (see
Fig.~\ref{fig:squid_3}). Thus, no current is diverted to the load arm and no
voltage drop is generated across the load.

If now the applied flux is near $\Phi = (n + \nicefrac{1}{2})\Phi_0$ (the ON
or OPEN state), then the resistance of SN is increased. If $R_\mathrm{SN} \gg
R_1 + Z_\mathrm{load}$, then the bias current is now diverted away from the
SQUID and into the load, thereby current-biasing the load.

We require a few conditions for this scheme to work. We must have
$R_\mathrm{SN}(\Phi=n\Phi_0) \ll R_1 + Z_\mathrm{load} \ll
R_\mathrm{SN}(\Phi=(n+\nicefrac{1}{2})\Phi_0)$, so that the current goes to
the correct spots in each $\Phi$ state. This is more easily accomplished if
the switch SQUID is in the weak-screening ($\beta_L \ll 1$) limit, since this
maximizes the resistance change between $\Phi$ states. The resistor $R_1$ is
required if the load is a superconductor, e.g. if it is another SQUID.

This scheme is fairly obviously extended. A new block of switch + load may be
appended in the dashed region. Each block requires its own coupling inductor
and only a single switch SQUID should be ON at a time. Thus we require $2N+2$
wires to drive $N$ loads. We note that a significant advantage of this scheme
is that only 2 lines directly couple into the bias circuit, compared to the
$2N+2$ of a standard multiplexing circuit. This reduces the noise power
coupling into the load. This scheme was described by Beyer \&
Drung\cite{beyer_squid_2008}.

Now let us consider the case where we voltage bias the bias circuit. The
general idea will be the same: only a single switch SQUID will be ON, and the
voltage drop will be across that SQUID, thereby voltage biasing the load. We
note that the coupling coil switches SN between the two $\Phi$ states of
Fig.~\ref{fig:squid_6}. The resistance of a single block is $R_\mathrm{block}
= R_\mathrm{SN} || (R_1 + Z_\mathrm{load})$. We should have $R_\mathrm{SN} \ll
R_1 + Z_\mathrm{load}$ in the $\Phi = n\Phi_0$ state and $R_\mathrm{SN} \gg
R_1 + Z_\mathrm{load}$ in the $\Phi = (n + \nicefrac{1}{2})\Phi_0$ state.

We will assume that all $N$ blocks are identical. The $N$ blocks make a
$N$-resistor voltage divider. The total impedance of the bias circuit is
$R_\mathrm{bias} = \sum_{i=0}^{N-1} R_{\mathrm{block},i} = (N-1)(R_1 +
Z_\mathrm{load}) + R_\mathrm{SN}^\mathrm{max}$, where the last equality
follows if only a single switch SQUID is ON. Then the voltage drop across
the ON SQUID is $V_\mathrm{ON} =
\frac{R_\mathrm{SN}^\mathrm{max}}{R_\mathrm{bias}}$ and across all of the OFF
SQUIDs is $V_\mathrm{OFF} = \frac{R_1 + Z_\mathrm{load}}{R_\mathrm{bias}}$, so
%
\begin{equation}
	\frac{V_\mathrm{ON}}{V_\mathrm{OFF}} = \frac{R_\mathrm{SN}^\mathrm{max}}{R_1 + Z_\mathrm{load}}.
\end{equation}
%
I.e., the voltage bias across the ON SQUID is significantly larger than the
voltage bias across the OFF SQUIDs. Relative to the bias voltage, we get
%
\begin{align}
	V_\mathrm{ON} & = \frac{R_\mathrm{SN}^\mathrm{max}}{(N-1)(R_1 + Z_\mathrm{load}) + R_\mathrm{SN}^\mathrm{max}} V_\mathrm{bias}
	\simeq \left[ 1 - (N-1) \frac{R_1 + Z_\mathrm{load}}{R_\mathrm{SN}^\mathrm{max}} \right] V_\mathrm{bias}\\
	V_\mathrm{OFF} & = \frac{R_1 + Z_\mathrm{load}}{(N-1)(R_1 + Z_\mathrm{load}) + R_\mathrm{SN}^\mathrm{max}} V_\mathrm{bias} \simeq \left( \frac{R_1 + Z_\mathrm{load}}{R_\mathrm{SN}^\mathrm{max}} \right) V_\mathrm{bias}
\end{align}
%
where the approximations hold if $R_\mathrm{SN}^\mathrm{max} \gg (N-1)(R_1 +
Z_\mathrm{load})$. As a further condition, we require that every switch
SQUID can comfortably support as supercurrent the full range of current that
will be passing through the load when it is ON. This is to ensure that we do
not inadvertently turn ON a switch SQUID that is supposed to be OFF.

We must limit cross-coupling between loads. For incoherent loads, we require
$V_\mathrm{ON} \gg \sqrt{N} V_\mathrm{OFF}$. For coherent loads, we require
$V_\mathrm{ON} \gg N V_\mathrm{OFF}$. This results in the conditions
%
\begin{subequations} \label{eq:fgs_conditions}
\begin{align}
	R_\mathrm{SN}^\mathrm{max} & \gg \sqrt{N} (R_1 + Z_\mathrm{load}) \quad \text{for incoherent loads} \\
	R_\mathrm{SN}^\mathrm{max} & \gg N (R_1 + Z_\mathrm{load}) \quad \text{for incoherent loads}.
\end{align}
\end{subequations}

The conditions for the switch are most easily satisfied for a switch SQUID in
the weak-screening ($\beta_L \ll 1$) limit. Furthermore, it is possible to use
a series array ($M$ DC SQUIDs wired in series) as the switch SQUID. This
distributes the voltage drop across the $M$ SQUIDs in the series array. Since
the $V$-$R$ curve (Fig.~\ref{fig:squid_6}) for a SQUID is
$R_\mathrm{SN}^\mathrm{OFF} = \frac{V}{I_S R} - \frac{1}{2}\left( \frac{V}{I_S
R} \right)^3 + \mathcal{O}\left( \left( \frac{V}{I_S R} \right)^5 \right)$ in
the OFF state, the total resistance of the $M$ SQUIDs is $M R(V/M) < R(V)$.
Additionally, since the $V$-$R$ curve is roughly constant in the ON state, the
resistance of the series array is $M R_\mathrm{SN}^\mathrm{max}$.
