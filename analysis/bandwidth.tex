\chapter{Relationship between Integration Time and Bandwidth}
\label{sec:bw}
\chaptermark{Inverse Seconds to Hz}

Suppose we integrate a signal $x(t)$ for a period of time $T$. This is
equivalent to filtering the signal with a rect filter,
%
\begin{equation}
    h(t) = \rect(t/T) = u(t + \nicefrac{T}{2}) - u(t - \nicefrac{T}{2}),
\end{equation}
%
where $u(t)$ is the Heaviside step function. In harmonic space, this filter is
%
\begin{equation}
    H(f) = F\{ h(t) \} = T \sinc{( f T )} = T \frac{\sin(\pi f T)}{\pi f T}
\end{equation}
%
where we define our fourier transform as
%
\begin{equation*}
    F \{ x(t) \} = \int_{-\infty}^\infty \dd f\ x(t) \exp(-2\pi i f t)
\end{equation*}

The bandwidth of a signal is conventionally defined as the distance in
frequency space between the first positive and first negative node. We note
that the sinc function is symmetric and has its first node at $f = 1/T$,
so the bandwidth is $B = 2/T$. Thus the relationship between integration time
and bandwidth is
%
\begin{align*}
    \unit[T]{seconds\ integration\ time} & \longleftrightarrow \unit[\frac{2}{T}]{Hz\ bandwidth} \\
    \unit[1]{second\ integration\ time} & \longleftrightarrow \unit[2]{Hz\ bandwidth}
\end{align*}
%
and so the conversion between seconds of integration time and Hz of bandwidth
is
%
\begin{equation}
    1 = \frac{\unit[1]{s}}{\unit[\frac{1}{2}]{Hz^{-1}}} = \frac{\unit[2]{s}}{\unit[1]{Hz^{-1}}}
\end{equation}

As a final note, the relationship between bandwidth and integration time is
inversely proportional. As the integration time increases, we integrate a
smaller bandwidth. This is intuitively correct, since as the bandwidth
decreases, the noise will decrease, and as the integration time increases, the
noise will decrease.


\chapter{Optimization of ILC Weights}
\label{cha:optimization_of_ilc_weights}

The ILC solution can be posed as the minimization problem
%
\begin{align*}
    \text{Minimize}\qquad & \varhT = \vw^T \bC \vw \quad \\
    \text{subject to}\qquad & \quad \bone^T \vw = 1
\end{align*}
%
with $\vw$ as the independent variable. This is amenable to solution by
Lagrange multipliers. Define the function
%
\begin{equation}
    L = \varhT + \lambda \left( \bone^T \vw - 1 \right).
\end{equation}
%
Then the solution to our optimization problem is given by the set of equations
%
\begin{subequations}
\begin{gather}
    \frac{\partial L}{\partial \vw^T} = 2\bC \vw + \lambda \bone = 0 \label{eq:dLdw} \\
    \bone^T \vw = 1 \label{eq:constraint}
\end{gather}
\end{subequations}
%
Solving Eq.~\eqref{eq:dLdw} for $\vw$
%
\begin{equation*}
    \vw = -\frac{\lambda}{2} \bC^{-1} \bone
\end{equation*}
%
and plugging it into Eq.~\eqref{eq:constraint} allows us to solve for
$\lambda$,
%
\begin{align}
    \bone^T \vw & = -\frac{\lambda}{2} \bone^T \bC^{-1} \bone = 1 \notag \\
    \lambda & = -2\left( \bone^T \bC^{-1} \bone \right)^{-1}
\end{align}
%
and then we may substitute into $\vw$,
%
\begin{equation}
    \vw^* = \frac{\bC^{-1} \bone}{\left( \bone^T \bC^{-1} \bone \right)} \label{eq:vwstar0}
\end{equation}