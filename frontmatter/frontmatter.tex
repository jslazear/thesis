%% FRONTMATTER
\begin{frontmatter}

% generate title
\maketitle

\begin{abstract}

%Abstract goes here.
The Inflationary Big Bang model of cosmology generically predicts the
existence of a background of gravitational waves due to Inflation, which
coupled into the B-mode power spectrum $C_\ell^\mathrm{BB}$ during the epochs
of Recombination and Reionization. A measurement of the primordial B-mode
spectrum would verify the reality of the Inflationary model and constrain the
allowed models of Inflation. In Chapter~1 we describe the background physics
of cosmology and Inflation, and the challenges involved with measuring the
primordial B-mode spectrum.

In Chapter~2 we describe the Primordial Inflation Polarization Explorer
(PIPER), a high-altitude balloon-borne microwave polarimeter optimized to
measure the B-mode spectrum on large angular scales. We examine the high level
design of PIPER and how it addresses the challenges presented in Chapter~1.

Following the high level design, we examine in detail the electronics
developed for PIPER, both for in-flight operations and for laboratory
development. In Chapter~3 we describe the Transition Edge Sensor (TES)
bolometers that serve as PIPER's detectors, analyze the Superconducting
Quantum Interference Device (SQUID) amplifiers and Mutli-channel Electronics
(MCE) detector readout chain, and finally present the characterization of both
detector parameters and noise of a single pixel device with a PIPER-like
(Backshort Under Grid, BUG) architecture to validate the detector design. In
Chapter~4 we present a description of the HKE electronics, used to measure all
non-detector science timestreams in PIPER, as well as flight housekeeping and
laboratory development. In addition to the operation of the HKE electronics,
we develop a model to quantify the performance of the HKE thermometry reader
(TRead).

A simple simulation pipeline is developed and used to explore the consequences
of imperfect foreground removal in Chapter~5. The details of estimating the
instrument noise as projected onto a sky map is developed also developed. In
particular, we address whether PIPER may be able to get significant science
return with only a fraction of its planned flights by optimizing the order
that the frequency bands are flown. Additionally, we look at how a spatially
varying calibration gain error would affect measurements of the B-mode
spectrum.

Finally, a series of appendices presents the physics of SQUIDs, develops
techniques for estimating noise of circuits and amplifiers, and introduces
techniques from control systems. In addition, a few miscellaneous results used
throughout the work are derived.


% We describe the Primordial Inflation Polarization
% Explorer (PIPER) instrument, a balloon-borne microwave polarimeter designed
% to measure the B-mode spectrum on angular scales $2 \leq \ell \leq 200$. PIPER
% will fly 4 frequency bands (200, 270, 350, 600 GHz) in both the Northern and
% Southern hemispheres in order to observe 90\% of the sky. A front-end

\vspace{1cm}

\noindent Primary Reader: Prof. Charles L. Bennett\\
Secondary Reader: Prof. Holland Ford

\end{abstract}

\begin{acknowledgment}

Developing the PIPER instrument has been, at its heart, a collaboration. None
of the accomplishments from the past 5 years would have been possible without
the support of my colleagues on PIPER. It has been a privilege to work with
and learn from the Goddard crew, and I would like to gratefully acknowledge
them. Al Kogut, our PI, has always kept the big picture in mind and pushed for
ambitious goals. He has always been a role model for how to think about
experimental physics and instrumentation. Paul Mirel has been a great resource
for how to convert something from an idea to a useful piece of metal on the
table. Between designing and building things, he somehow manages to clean and
organize the lab, and know where everything is! Eric Switzer has always had
useful input and advice, given freely. In a short time, he has become an
invaluable member of the PIPER team. Dan Sullivan has been willing to jump
into the cryo and ADR trenches with me, and he uniquely understands the great
joy involved with getting the ADRs working.

I would like to thank the other members of our electronics team, Jamie
Hinderks and Luke Lowe. Luke has an incomparable attention to detail that has
prevented many costly errors and improved our designs, and he is the ultimate
reference on how all of our systems are wired up. A special thanks to Jamie,
who has spent countless late Friday nights with me working on designs and
discussing electronics and signal processing and physics and many other
things. A huge part of my approach to electronics and experimentation was
developed following his example. Combined with his immense skill and knowledge
was his enthusiasm and willingness to tell you just how much he loved what you
had done.

I have also had the great benefit of working with many other expert scientists
and engineers at GSFC. Ed Wollack, Dale Fixsen, Harvey Moseley, Dave Chuss,
and Dominic Benford have all been willing to answer my questions and
contributed to my understanding of physics. Christine Jhabvala, Tim Miller,
and other members of the DDL have fabricated our detectors. Elmer Sharp and
Johannes Staguhn have lended their expertise with the cryo facilities in B34.
Mark Kimball, Peter Shirron, Ed Canavan, and others with the Cryo Branch have
provided guidance on and helped troubleshoot the ADRs. Brad Johnson advocated
for me early on and started me on the pathway toward detectors and
electronics.

PIPER has been fortunate to have the assistance of technicians Samelys
Rodriguez and Amy Weston. Without them, my single pixel measurements would
never have been completed in time. I would especially like to recognize Sam,
who makes wirebonding look just so easy and whose interminable work ethic
ensured our cryo packages were always ready in time. I have also worked with
myriad excellent interns over the years. Some of them, like Sam Pawlyk and
Peter Taraschi, we even managed to convince to stay on.

Although I spent most of my time at GSFC, I had the advantage of working with
the people from my home institution. I would especially like to thank my
advisor Chuck Bennett, who has looked out for me for the last few years. Every
time we met I gained invaluable insight and had a better picture of what I
needed to do. Without him, it's frightening to imagine how many more years it
would take to publish this. I'd also like to thank my advisory committee, Toby
Marriage and Holland Ford, for keeping me on track.

I'd also like to thank my graduate student and post doc colleagues, Joseph
Eimer, Aamir Ali, Maxime Rizzo, and Dominik Gothe, as well as my officemates,
particularly Rafael and Gabriella, for reminding me that we're not in this
alone.

My family has always supported me, even though for the last 6 years they
didn't have much of a sense of what I was doing, mostly because I didn't call
say hi enough.

My wife has been my companion on this adventure to the other side of the
country. She tolerated me when I was distracted by work, reminded me to relax
wen problems seemed insurmountable, and was willing to talk about all the
random things I have worked on through the years without looking too bored.
More than that, her thoughts are always coherent and well-thought out, even
when my own aren't, and she's my secret encyclopaedia on Fourier transforms
--- even if she does them in the wrong basis. Most of all, she has always
provided a place to go home to at the end of the day.

\end{acknowledgment}

\begin{dedication}

For Jen.

\end{dedication}

% generate table of contents
\tableofcontents

% generate list of tables
\listoftables

% generate list of figures
\listoffigures

\end{frontmatter}
