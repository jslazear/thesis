#!/bin/python

"""
generate_images.py
jlazear
1/22/15

Generates images used in the docs files.

Example:

$ python generate_images.py
"""
__version__ = 20150122
__releasestatus__ = 'beta'


import os
import sys
import argparse

import wget
import numpy as np
import healpy as hp
import matplotlib.pyplot as plt
import scipy.signal
from scipy.integrate import quad, simps


imgpath = os.path.dirname(os.path.abspath(__file__)) + '/'


# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("--regenerate",
                    help="force the regeneration of all figures",
                    action="store_true")
args = parser.parse_args()
force = bool(args.regenerate)

strtemplate = "Saving {0} to {1}\r\n"
strexists = "File {0} already exists."

# -------------------------------------
# ---------- Generate images ----------
# -------------------------------------

# filter data files
fnames = [imgpath + 'antialias.txt', imgpath + 'fir1.txt',
          imgpath + 'fir66.txt', imgpath + 'fir66_nogap.txt',
          imgpath + 'envelope.txt']
allthere = all(map(os.path.isfile, fnames))
if allthere and not force:
    for fname in fnames:
        print strexists.format(fname)
else:
    zfreqs = np.logspace(-1., np.log10(4.e3), 512)*np.pi/2.e3

    # fir66.txt
    N_sum = 66
    N_gap = 59
    sign = -1.
    betas = np.array([1.]*N_sum + [0.]*N_gap + [1.*sign]*N_sum
                      + [0.]*N_gap)/(2*N_sum)
    zfreqs_temp = zfreqs + 0.
    freqs, hs = scipy.signal.freqz(betas, worN=zfreqs_temp)
    freqs *= 2.e3/np.pi
    np.savetxt(imgpath + 'fir66.txt', np.array((freqs, np.abs(hs))).T)

    # fir66_nogap.txt
    N_sum2 = 66
    N_gap2 = 0
    sign = -1.
    betas2 = np.array([1.]*N_sum2 + [0.]*N_gap2 + [1.*sign]*N_sum2
                      + [0.]*N_gap2)/(2*N_sum2)
    zfreqs_temp = zfreqs + 0.
    freqs2, hs2 = scipy.signal.freqz(betas2, worN=zfreqs_temp)
    freqs2 *= 2.e3/np.pi
    np.savetxt(imgpath + 'fir66_nogap.txt', np.array((freqs2, np.abs(hs2))).T)

    # fir1.txt
    N_sum3 = 1
    sign = -1.
    betas3 = np.array([1.]*N_sum3 + [1.*sign]*N_sum3)/(2*N_sum3)
    zfreqs_temp = zfreqs + 0.
    freqs3, hs3 = scipy.signal.freqz(betas3, worN=zfreqs_temp)
    freqs3 *= 2.e3/np.pi
    np.savetxt(imgpath + 'fir1.txt', np.array((freqs3, np.abs(hs3))).T)

    # envelope.txt
    N_sum4 = 33
    N_gap4 = 0
    sign = 1.
    betas4 = np.array([1.]*N_sum4 + [0.]*N_gap4 + [1.*sign]*N_sum4
                      + [0.]*N_gap4)/(2*N_sum4)
    zfreqs_temp = zfreqs + 0.
    freqs4, hs4 = scipy.signal.freqz(betas4, worN=zfreqs_temp)
    freqs4 *= 2.e3/np.pi
    np.savetxt(imgpath + 'envelope.txt', np.array((freqs4, np.abs(hs4))).T)

    # antialias.txt
    bessel_zpk = scipy.signal.besselap(3)
    blti = scipy.signal.lti(*bessel_zpk)

    zfreqs_temp = zfreqs + 0.
    freqs_bessel, hs_bessel = scipy.signal.freqs(blti.num, blti.den,
                                                 worN=zfreqs_temp)
    freqs_bessel *= 920.
    np.savetxt(imgpath + 'antialias.txt',
               np.array((freqs_bessel, np.abs(hs_bessel))).T)

fname = imgpath + 'sine_excitation.txt'
if os.path.isfile(fname) and not force:
    print strexists.format(fname)
else:
    n_ch = 16          # Number of channels to measure
    T_frame = 1.       # Frame period
    N_hp = 125         # Number of ticks per half-period

    T0 = T_frame/n_ch  # Single-channel period, assuming 1 second total period
    w0 = 2*np.pi/T0    # Single-channel angular frequency
    T60 = 1./60.       # 60 Hz period
    w60 = 2*np.pi/T60  # 60 Hz angular frequency

    amp = np.sqrt(2/T0)   # A = sqrt(2/T0) gives time-averaged power amplitude of 1 over a period of T0

    f1 = lambda t: -np.sin(w0*t)**2 * amp
    f2 = lambda t: np.sin(w0*t)*np.sin(w60*t)
    f3 = lambda t: np.sin(2*w0*t)*np.sin(w60*t)

    result1 = quad(f1, 0, T0)[0]
    result2 = quad(f2, 0, T0)[0]
    result3 = quad(f3, 0, T0)[0]

    # print result1, result2, result3
    k0 = 1./result1
    k1 = -result2/(result1*result3)
    # print "k0 = ", k0
    # print "k1 = ", k1

    ct = lambda t: amp * np.sin(w0*t)
    ht = lambda t: k0*np.sin(w0*t) + k1*np.sin(2*w0*t)
    ht0 = lambda t: k0*np.sin(w0*t)

    ts = np.linspace(0, T0)
    cts = ct(ts)
    hts = ht(ts)
    ht0s = ht0(ts)

    tosave = np.array([1000.*ts, cts, hts, ht0s]).T

    np.savetxt(fname, tosave)

fname = imgpath + 'power_spectrum_comparison.txt'
if os.path.isfile(fname) and not force:
    print strexists.format(fname)
else:
    # ------- 60 Hz Rejection Sine Wave Filter --------
    N_sum = 66.
    N_gap = 59.
    n_ch = 16.

    N_chop = N_sum + N_gap
    N_tot = 2*N_chop
    ns = np.arange(N_tot, dtype='float')
    w0d = 2*np.pi/N_tot

    T0 = 1./n_ch
    w0 = 2*np.pi/T0
    T60 = 1./60.
    w60 = 2*np.pi/T60
    dT = T0/N_tot

    f1 = lambda t: -np.sin(w0*t)**2 * amp
    f2 = lambda t: np.sin(w0*t)*np.sin(w60*t)
    f3 = lambda t: np.sin(2*w0*t)*np.sin(w60*t)

    result1 = quad(f1, 0, T0)[0]
    result2 = quad(f2, 0, T0)[0]
    result3 = quad(f3, 0, T0)[0]

    k0 = 1./result1
    k1 = -result2/(result1*result3)

    f4 = lambda t: (k0*np.sin(w0*t) + k1*np.sin(2*w0*t))**2
    result4 = quad(f4, 0, T0)[0]
    print "(Std Sine 60 Hz-rejecting)/(Std ideal) = ", np.sqrt(result4)

    betas_60 = (k0*np.sin(w0d*ns) + k1*np.sin(2*w0d*ns)) * dT
    carrier2 = amp*np.sin(w0d*ns)

    print "sine wave gain = ", np.convolve(betas_60, carrier2, mode="valid")# * dT

    zfreqs_temp = np.logspace(-1., np.log10(4.e3), 5120)*np.pi/2.e3 # zfreqs + 0.
    freqs_60, hs_60 = scipy.signal.freqz(betas_60, worN=zfreqs_temp)
    freqs_60 *= 2.e3/np.pi

    # ------- Square Wave Filter --------
    N_sum = 66
    N_gap = 59
    t_sum = T0 * N_sum/N_tot
    t_gap = T0 * N_gap/N_tot
    ts = T0 * ns/N_tot
    amp_sq = 1./np.sqrt(T0)
    # sign = -1
    # carrier_sq = 1./np.sqrt(N_gap + N_sum) * np.array([1.]*N_gap + [1.]*N_sum + [1.*sign]*N_gap + [1.*sign]*N_sum)
    # betas_sq = 1./N_sum * np.sqrt((N_sum + N_gap)/2) * np.array([0.]*N_gap + [1.]*N_sum + [0.]*N_gap + [1.*sign]*N_sum)/(-2*N_sum)
    carrier_sq = amp_sq * scipy.signal.square(w0d*ns)
    # carrier_sq_func = lambda N: 1./np.sqrt(T0) if (N < N_sum + N_gap) else -1./np.sqrt(T0)
    def beta_sq_func(t, Ts, Tg):
        T0 = 2*(Ts + Tg)
        a = np.sqrt(T0)/(2*Ts)
        if t < Tg:
            return 0.
        elif t < Ts + Tg:
            return -a
        elif t < Ts + 2*Tg:
            return 0
        else:
            return a

    betas_sq = np.array([beta_sq_func(t, t_sum, t_gap) for t in ts]) * dT
    # carrier_sq = np.array([carrier_sq_func(n) for n in ns])
    # betas_60 = (k0*np.sin(w0d*ns) + k1*np.sin(2*w0d*ns)) * dT
    # carrier2 = np.sin(w0d*ns)

    print "square wave gain = ",  np.convolve(betas_sq, carrier_sq, mode="valid")# * dT

    zfreqs_temp = np.logspace(-1., np.log10(4.e3), 5120)*np.pi/2.e3 # zfreqs + 0.
    freqs_sq, hs_sq = scipy.signal.freqz(betas_sq, worN=zfreqs_temp)
    freqs_sq *= 2.e3/np.pi

    # Save both filters
    tosave = np.array([freqs_sq, np.abs(hs_60)**2, np.abs(hs_sq)**2]).T
    np.savetxt(fname, tosave)

    Var_sq = simps(np.abs(hs_sq)**2, freqs_sq)
    Var_60 = simps(np.abs(hs_60)**2, freqs_60)


    print "[Constant Power Dissipation] (Std Sine)/(Std Square) ", np.sqrt(Var_60/Var_sq)



    # plt.plot(freqs_sq, np.abs(hs_sq))
    # plt.xlim(xmax=2000.)
    # plt.ylim(ymin=1.e-5)
    # plt.yscale('log')
    # plt.legend(loc='lower left')
    # plt.xscale('log')
    # plt.xlabel('Frequency, f (Hz)', fontsize=20)
    # plt.ylabel('Transfer Function |H(f)| (unitless)', fontsize=20)
    # fig = plt.gcf()
    # fig.set_size_inches(16,6)
    # plt.axvline(60., ls='--')

    # print "\int |H(f)|^2 df (Square Wave, hs_sq) = ", simps(np.abs(hs_sq), freqs_sq)

    # plt.plot(freqs_60, np.abs(hs_60))
    # plt.xlim(xmax=2000.)
    # plt.ylim(ymin=1.e-5)
    # plt.yscale('log')
    # plt.legend(loc='lower left')
    # plt.xscale('log')
    # plt.xlabel('Frequency, f (Hz)', fontsize=20)
    # plt.ylabel('Transfer Function |H(f)| (unitless)', fontsize=20)
    # fig = plt.gcf()
    # fig.set_size_inches(16,6)
    # plt.axvline(60., ls='--')

    # print "\int |H(f)|^2 df (60 Hz Sine Rejection, hs_60) = ", simps(np.abs(hs_60), freqs_60)


# # inductances.png
# fname = imgpath + 'inductances.png'
# if os.path.isfile(fname) and not force:
#     print strexists.format(fname)
# else:
#     f_Ny = 10.e3
#     tau_Ny = 1./(2*np.pi*f_Ny)
#     f_VPM = 3.
#     tau_VPM = 1./(2*np.pi*f_VPM)
#     # print "tau_Ny =", tau_Ny
#     # print "tau_VPM =", tau_VPM

#     f_VPM = 3.  # Hz
#     w_VPM = 2*np.pi*f_VPM  # rad/s
#     tau_VPM = 1/w_VPM  # s/rad

#     F = 10.

#     R_L = 2.e-3  # 2 mOhm
#     R0 = 8.e-3  # 8 mOhm
#     LI = 25.  # Loop gain
#     beta_I = 0.3  # Dimensionless current sensitivity
#     tau = np.linspace(0.002, 0.02)  # seconds, lower and upper bounds (from Benford)

#     tau_I = tau/(1 - LI)

#     Lmin = ((R_L + R0*(1 + beta_I)) - tau_Ny/tau * (LI*(R0-R_L)+ R_L + R0*1 + beta_I))/(1./tau_Ny - 1/tau_I)

#     Lcritminus = (LI * (3 + beta_I - R_L/R0) + (1 + beta_I+ R_L/R0)
#                   - 2*np.sqrt(LI*(2 + beta_I)*(LI*(1 - R_L/R0)
#                                                + (1 + beta_I + R_L/R0)))) * (R0*tau)/((LI - 1)**2)

#     fig, ax = plt.subplots()
#     ax.plot(tau*1.e3, Lmin*1.e9, label=r'$L_\mathrm{min}$')
#     ax.plot(tau*1.e3, Lcritminus*1.e9, label=r'$L_{\mathrm{crit}-}$')
#     ax.axhline(400., ls='--')
#     ax.set_xlabel(r'$\tau = C/G$ (ms)', fontsize=24)
#     ax.set_ylabel(r'Inductance (nH)', fontsize=24)
#     ax.legend(loc='upper left', fontsize=20)

#     print strtemplate.format('TES Inductances', fname)
#     fig.savefig(fname)
#     fig.clf()
