import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import quad, simps
import scipy.constants
import scipy.signal

kB = scipy.constants.k
num_row = 41
f_MCE = 50.e6
T_MCE = 1./f_MCE
T_row = 100*T_MCE
T_tot = num_row*T_row


# PIPER laboratory parameters
fs = 4.e3  # 4 kHz sampling rate
N_SUM = 66 # SUM_TICKS = 66
N_GAP = 59
t_SUM = N_SUM/fs
f_SUM = 1./(2*t_SUM)
t_GAP = N_GAP/fs
# t0 = 3./2.*t_SUM + N_GAP/fs
t0 = t_SUM + t_GAP
t_SUM, f_SUM, t0


# Integrate filter transfer function to get effective bandwidth
zfreqs = np.logspace(-1., np.log10(4.e3), 5120)*np.pi/2.e3
N_sum = 66
N_gap = 59
sign = -1
betas = np.array([0.]*N_gap + [1.]*N_sum + [0.]*N_gap + [1.*sign]*N_sum)/(2*N_sum)
zfreqs_temp = zfreqs + 0.
freqs, hs = scipy.signal.freqz(betas, worN=zfreqs_temp)
freqs *= 2.e3/np.pi
f_filt = simps(np.abs(hs[freqs < 920])**2, freqs[freqs < 920])


# Load test data
data = np.loadtxt('hke_20120103_000_new.txt', skiprows=1, unpack=False)
data_names = ['ch', 'R(nom)', 'ADAC', 'GAIN', 'Demod (avg)', 'Demod (std)', 'NSUM', 'R(avg)', 'R(std)', 'N']
ind = np.lexsort((data[:,3], data[:,2]))
sdata = data[ind]
adac_unique = np.unique(sdata[:,2])
gain_unique = np.unique(sdata[:,3])
sdata_a = [sdata[sdata[:, 2] == a] for a in adac_unique]
sdata_ag = np.array([[s[s[:,3] == g] for g in gain_unique] for s in sdata_a])  # list of list of 2D numpy array
                                                                               # 1st index = adac, 2nd index = gain
# mya = sdata_ag[3, 4, :, (1, 5)]
# plt.plot(mya[0], mya[1], 'o')
# plt.xscale('log')



class Noise(object):
    def __init__(self, names, stds):
        self.names = names
        try:
            self.stds = stds
            self.vars = [v*v for v in stds]
        except TypeError:
            self.stds = [stds]
            self.vars = [stds*stds]
        self.total = np.sqrt(np.sum(np.array(self.vars)))

    def add_stage(self, gain=1., newnames=[], newstds=[]):
        names = self.names + newnames
        values = [gain*v for v in self.stds] + newstds
        return Noise(names, values)

    def __add__(self, other):
        names = self.names + other.names
        stds = self.stds + other.stds
        return Noise(names, stds)

    def __mul__(self, other):
        names = self.names
        stds = [other*v for v in self.stds]
        return Noise(names, stds)

    def __rmul__(self, other):
        return self.__mul__(other)

    def __str__(self):
        printstr = "{0:>20} {1:<20}"
        toret = ''
        toret += printstr.format('Source', 'Std Dev (nV/sqrt(Hz))') + '\n'
        toret += printstr.format('-'*18, '-'*18) + '\n'
        for i in range(len(self.names)):
            name = self.names[i]
            std = self.stds[i]
            toret += printstr.format(name, std*1.e9) + '\n'
        toret += printstr.format('-'*20, '-'*20) + '\n'
        toret += printstr.format('TOTAL', np.sqrt(np.sum(self.vars))*1.e9)
        return toret

    def __repr__(self):
        return self.__str__()

def parallel(*args):
    a = np.array(args)
    a = 1./a
    a = np.sum(a)
    return 1./a


# ADR444 = Voltage Reference
dV_ADR444 = 78.6e-9  # 78.6 nV/sqrt(Hz)

# OPA2277 = General purpose op amp
dV_OPA2277 = 12.e-9  # 8 nV/sqrt(Hz) @ 10 Hz. Use this over @ 1 kHz value since f_SUM ~ 30 Hz
dI_OPA2277 = 0.2e-12 # 0.2 pA/sqrt(Hz)

# AD8221 = TRead_Standard InAmp, AD620 = old TRead_Standard InAmp
dV_AD620 = 10.e-9    # 9 nV/sqrt(Hz)
dV_AD8221 = 8.e-9    # 8 nV/sqrt(Hz)
dV_AD8221 = dV_AD620  #DELME for comparing against data with old InAmp
# dI_AD8221 = see cell below
dVout_AD620 = 72.e-9  # 72 nV/sqrt(Hz) @ 1 kHz, no curve supplied.
# dVout_AD8221 = 75.e-9  # 75 nV/sqrt(Hz) @ 1 kHz, no curve supplied.
dVout_AD8221 = dVout_AD620  #DELME for comparing against data with old InAmp

# AD8429 = TRead_LR InAmp
dV_AD8429 = 1.e-9    # 1 nV/sqrt(Hz)
# dI_AD8429 = see cell below
dVout_AD8429 = 45.e-9  # 45 nV/sqrt(Hz) @ 1 kHz, no curve supplied.
# AD8429 data sheet supplies RTI voltage noise curve, could make better estimate

T_room = 293.15      # Room temperature ~ 20 degC
T = T_room # 1.               # test device temperature ~ 1 K

# AD7685 = ADC
dD_AD7685 = 0.55      # 0.6 LSB = counts (0.55 measured?)


# AD8221 equivalent white current noise

# Model current noise w/ 1/f knee
fs = np.array([2., 200.])
dI0 = 40.e-15
dIs = np.array([400., 50.])*1.e-15 - dI0
logfs = np.log(fs)
logdIs = np.log(dIs)
alpha, beta = np.polyfit(logfs, logdIs, 1)
A = np.exp(beta)
n = alpha
dI_func = lambda f: A*(f**n) + dI0

# Demodulation filter function |H(w)|^2
# filt_func = lambda w: np.sin(w*t_SUM/2.)**2 * np.cos(w*t0/2.)**2/((w*t_SUM/2.)**2)
filt_func = lambda w: np.sin(w*t_SUM/2.)**2 * np.sin(w*t0/2.)**2/((w*t_SUM/2.)**2)

# Integrate current noise density \int_0^\infty |H(\omega)|^2 \delta I(\omega)^2 \dd \omega
h2f = lambda f: filt_func(2*np.pi*f) * dI_func(f)**2
dI_AD8221 = np.sqrt(quad(h2f, 0, np.infty, limit=500)[0]/f_filt) # Divide by effective bandwidth and sqrt to get units right
print "dI_AD8221 = {0} fA/sqrt(Hz)".format(dI_AD8221*1.e15)


# AD8429 equivalent white current noise

# Model current noise w/ 1/f knee
fs = np.array([2., 200.])
dI0 = 1.5e-12
dIs = np.array([11., 2.])*1.e-12 - dI0
logfs = np.log(fs)
logdIs = np.log(dIs)
alpha, beta = np.polyfit(logfs, logdIs, 1)
A = np.exp(beta)
n = alpha
dI_func = lambda f: A*(f**n) + dI0

# Demodulation filter function |H(w)|^2
# filt_func = lambda w: np.sin(w*t_SUM/2.)**2 * np.cos(w*t0/2.)**2/((w*t_SUM/2.)**2)
filt_func = lambda w: np.sin(w*t_SUM/2.)**2 * np.sin(w*t0/2.)**2/((w*t_SUM/2.)**2)

# Integrate current noise density \int_0^\infty |H(\omega)|^2 \delta I(\omega)^2 \dd \omega
h2f = lambda f: filt_func(2*np.pi*f) * dI_func(f)**2
dI_AD8429 = np.sqrt(quad(h2f, 0, np.infty, limit=500)[0]/f_filt) # Divide by effective bandwidth and sqrt to get units right
print "dI_AD8429 = {0} pA/sqrt(Hz)".format(dI_AD8429*1.e12)


# AD620 equivalent white voltage noise

# Model current noise w/ 1/f knee
fs = np.array([1., 10.])
dV0 = 9.e-9
dVs = np.array([21., 10.])*1.e-9 - dV0
logfs = np.log(fs)
logdVs = np.log(dVs)
alpha, beta = np.polyfit(logfs, logdVs, 1)
A = np.exp(beta)
n = alpha
dV_func = lambda f: A* (f**n) + dV0

# Demodulation filter function |H(w)|^2
# filt_func = lambda w: np.sin(w*t_SUM/2.)**2 * np.cos(w*t0/2.)**2/((w*t_SUM/2.)**2)
filt_func = lambda w: np.sin(w*t_SUM/2.)**2 * np.sin(w*t0/2.)**2/((w*t_SUM/2.)**2)

# Integrate current noise density \int_0^\infty |H(\omega)|^2 \delta I(\omega)^2 \dd \omega
h2f = lambda f: filt_func(2*np.pi*f)* dV_func(f)**2
dV_AD620 = np.sqrt(quad(h2f, 0, np.inf, limit=500)[0]/f_filt) # Divide by effective bandwidth and sqrt to get units right
print "dV_AD620 = {0} nV/sqrt(Hz)".format(dV_AD620*1.e9)
dV_AD8221 = dV_AD620  # re-assign our voltage noise to use the old amplifier value


##################
# TREAD_STANDARD #
##################

# TRead_Standard
R_45 = 20.e3   # 20 kOhm
R_47 = 20.e3   # 20 kOhm
R_50 = 49.9e3  # 49.9 kOhm
R_52 = 698.    # 698 Ohms
R_ADG419 = 45. # 45 Ohms for +-15 V, +5V biased
G_ref = R_52/(R_50 + R_52)  # divider gain
V_ref = 4.096  # 4.096 V voltage reference
deltaV = 4*V_ref
R_FB = 7.e3    # 7 kOhm
# G_dac = 64./513.      # MDAC gain, for 64 nA
# ADAC = G_dac*(2**16)
G_pre = 100.
# G = 16.
R_P = 100. + 680.     # DG407DJ mux resistance + protection resistor
R_L = 10.e3 + 100.e3  # protection resistor + bias resistor
R_T = 10.e3    # 10 kOhm
R_m1 = 20.e3   # 20 kOhm

ADACs = adac_unique
G_dacs = ADACs/2**16
Gs = gain_unique

R_Ts = np.logspace(0, 6, 50)  # 1 Ohm -> 1 MOhm
dV_Gs = []
dRT_Gs = []
dDemod_Gs = []

for G_dac in G_dacs:
    ADAC = G_dac*(2**16)
    dvs1 = []
    drts1 = []
    dds1 = []
    for G in Gs:
        dvs2 = []
        drts2 = []
        dds2 = []

        # A
        names = ['U14']
        stds = [dV_ADR444]
        dV_A = Noise(names, stds)
        #dV_A

        # B
        names = ['U15B_V', 'U15B_I', 'U15B_R']
        stds = [2*dV_OPA2277, R_45*dI_OPA2277, np.sqrt(4*kB*T_room*(R_45 + R_47))]
        dV_B = dV_A.add_stage(1., names, stds)
        # dV_B

        # C
        names = ['divider_R']
        stds = [np.sqrt(4*kB*T_room*parallel(R_52, R_50 + R_ADG419))]
        dV_C = dV_B.add_stage(G_ref, names, stds)
        #dV_C

        # IN
        names = ['U15A_V', 'U15A_I']
        stds = [dV_OPA2277, parallel(R_52, R_50 + R_ADG419)*dI_OPA2277]
        dV_in = dV_C.add_stage(1., names, stds)
        #dV_in

        # DAC
        names = ['U18A_V', 'U18A_I', 'DAC_FB_R']
        stds = [2*dV_OPA2277, R_FB*dI_OPA2277, np.sqrt(4*kB*T_room*R_FB)]
        dV_DAC = dV_in.add_stage(G_dac, names, stds)
        #dV_DAC

        # Bias circuit
        dV_m1 = Noise(['U18B_V', 'U18B_I', 'U18B_R'], [2*dV_OPA2277, R_m1*dI_OPA2277, np.sqrt(4*kB*T_room*2*R_m1)])
        dV_RL = Noise(['Johnson_RL'], np.sqrt(4*kB*T_room*R_L))
        dV_amp = Noise(['InAmp_V'], dV_AD8221)
        dV_ampout = Noise(['InAmp_out'], [dVout_AD8221])
        dI_inamp = Noise(['InAmp_I'], [dI_AD8221])

        # ADC Noise back-referenced to ADC input
        dV_ADC = Noise(['ADC'], (20.48/65535.) * dD_AD7685/np.sqrt(2000.))
        #dV_ADC

        for R_T in R_Ts:
            dV_RT = Noise(['Johnson_RT'], np.sqrt(4*kB*T*R_T))
            g1 = G_pre*(2*R_L/(2*R_L + R_T))
            g2 = G_pre*(R_T/(2*R_L + R_T))
            g3 = np.sqrt(2)*G_pre*(R_P + 0.5*(2*R_L*R_T)/(2*R_L + R_T))
            dV_G = (G*(dV_ampout
                       + G_pre*dV_amp
                       + g1*dV_RT
                       + g2*(np.sqrt(2)*dV_DAC + dV_m1 + np.sqrt(2)*dV_RL)
                       + g3*dI_inamp)
                   + dV_ADC)
            dvs2.append(dV_G)

            alpha = 2*G*G_pre*G_ref*G_dac*V_ref
#             V_ref = 4.096  # Volts
            V_G = G*G_pre*(G_dac*G_ref*V_ref) * 2/(1 + 2*R_L/R_T)
            drtg = 2.*R_L*alpha*ADAC*deltaV/(alpha*ADAC - V_G)**2 * dV_G
            drts2.append(drtg)

            beta = 2**16/20.48*2*N_SUM
            ddg = beta*dV_G
            dds2.append(ddg)
        dvs1.append(dvs2)
        drts1.append(drts2)
        dds1.append(dds2)
    dV_Gs.append(dvs1)
    dRT_Gs.append(drts1)
    dDemod_Gs.append(dds1)



# ADC-input referred voltages (V_G)
# Collate individual components for various R_T values
dV_Gs_tot = []
dV_Gs_components = []
for i in range(len(G_dacs)):
    ddgtot1 = []
    ddgc1 = []
    for j in range(len(Gs)):
        ddgtot2 = np.array([dvg.total for dvg in dV_Gs[i][j]])
        ddgtot1.append(ddgtot2)
        ddgc2 = []
        for k in range(len(dV_Gs[i][j][0].names)):
            toadd = np.array([dvg.stds[k] for dvg in dV_Gs[i][j]])
            ddgc2.append(toadd)
        ddgc1.append(ddgc2)
    dV_Gs_tot.append(ddgtot1)
    dV_Gs_components.append(ddgc1)


# R_T values
# Collate individual components for various R_T values
dRT_Gs_tot = []
dRT_Gs_components = []
for i in range(len(G_dacs)):
    ddgtot1 = []
    ddgc1 = []
    for j in range(len(Gs)):
        ddgtot2 = np.array([dvg.total for dvg in dRT_Gs[i][j]])
        ddgtot1.append(ddgtot2)
        ddgc2 = []
        for k in range(len(dRT_Gs[i][j][0].names)):
            toadd = np.array([dvg.stds[k] for dvg in dRT_Gs[i][j]])
            ddgc2.append(toadd)
        ddgc1.append(ddgc2)
    dRT_Gs_tot.append(ddgtot1)
    dRT_Gs_components.append(ddgc1)


# Demodulated values
# Collate individual components for various R_T values
dDemod_Gs_tot = []
dDemod_Gs_components = []
for i in range(len(G_dacs)):
    ddgtot1 = []
    ddgc1 = []
    for j in range(len(Gs)):
        ddgtot2 = np.array([dvg.total for dvg in dDemod_Gs[i][j]])
        ddgtot1.append(ddgtot2)
        ddgc2 = []
        for k in range(len(dDemod_Gs[i][j][0].names)):
            toadd = np.array([dvg.stds[k] for dvg in dDemod_Gs[i][j]])
            ddgc2.append(toadd)
        ddgc1.append(ddgc2)
    dDemod_Gs_tot.append(ddgtot1)
    dDemod_Gs_components.append(ddgc1)



# ADC-input referred voltage noise
# Plot significant components vs R_T
threshhold = 0.5  # Plot threshhold for individual components, i.e. if ever comp/tot > threshhold, plot it
print "1st Index (ADAC) = ", adac_unique
print "2nd Index (Gain) = ", gain_unique
i = 2
j = 3
print "Using: ADAC = {0}, Gain = {1}".format(adac_unique[i], gain_unique[j])
print "Excitation I = {0} nA".format(G_dacs[i]*513.)

ddgs = dV_Gs[i][j]
ddgt = dV_Gs_tot[i][j]
ddgc = dV_Gs_components[i][j]
# vfact = np.sqrt(f_SUM)*1.e9
vfact = 1.e9/(100*gain_unique[j]) # np.sqrt(1.0736*920.)
fig, ax = plt.subplots(figsize=(12, 9), dpi=150)
ax.plot(R_Ts, ddgt*vfact, 'k--', label='Total', linewidth=2)
for k in range(len(ddgs[0].names)):
    name = ddgs[0].names[k]
    value = ddgc[k]
    if np.any(value/ddgt > threshhold):
        ax.plot(R_Ts, ddgt*0 + value*vfact, label=name, linewidth=2)

mya = sdata_ag[i, j, :, (1, 5)]  # 1st -> adac, 2nd -> gain, 3rd -> row, 4th -> column
# col 1 = expected resistance, col 5 = Demod (std)
# ax.plot(mya[0], mya[1], 'o', label='Data')

lgd = ax.legend(bbox_to_anchor=(1.35, 1), prop={'size':20})
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlabel(r'$R_T$ ($\Omega$)', fontsize=20)
ax.set_ylabel(r'RMS V Noise (nV)', fontsize=20)
ax.set_title(r'TRead_Standard, G = {0}, I = {1} $\mu$A'.format(int(gain_unique[j]),
                                                               int(np.round(G_dacs[i]*513))), fontsize=24)
# ax.set_ylim(ymin=np.min(dV_Gs_tot)*vfact/10.)
# ax.set_ylim(ymin=1.e-1)
ax.tick_params(labelsize=16)
fig.savefig('noise_model.png', dpi=150, bbox_extra_artists=(lgd,), bbox_inches='tight')


# Normalized demod (D = demod/2N_SUM) referred noise
# Plot significant components vs R_T
threshhold = 0.5  # Plot threshhold for individual components, i.e. if ever comp/tot > threshhold, plot it
print "1st Index (ADAC) = ", adac_unique
print "2nd Index (Gain) = ", gain_unique
i = 2
j = 3
print "Using: ADAC = {0}, Gain = {1}".format(adac_unique[i], gain_unique[j])
print "Excitation I = {0} nA".format(G_dacs[i]*513.)

ddgs = dDemod_Gs[i][j]
ddgt = dDemod_Gs_tot[i][j]
ddgc = dDemod_Gs_components[i][j]
# vfact = np.sqrt(1.0736*920.)
vfact = np.sqrt(f_filt)  # Use FIR equivalent bandwidth
fig, ax = plt.subplots(figsize=(12, 9), dpi=150)
ax.plot(R_Ts, ddgt*vfact, 'k--', label='Total', linewidth=2)
for k in range(len(ddgs[0].names)):
    name = ddgs[0].names[k]
    value = ddgc[k]
    if np.any(value/ddgt > threshhold):
        ax.plot(R_Ts, ddgt*0 + value*vfact, label=name, linewidth=2)

mya = sdata_ag[i, j, :, (1, 5, 9)]  # 1st -> adac, 2nd -> gain, 3rd -> row, 4th -> column
# col 1 = expected resistance, col 5 = Demod (std), col 6 = N_pts
_, caps, _ = ax.errorbar(mya[0], mya[1], yerr=mya[1]/np.sqrt(2*(mya[2] - 1)), fmt='o', label='Data', capsize=5)
for cap in caps:
    cap.set_markeredgewidth(2)
# ax.plot(mya[0], mya[1], 'o', label='Data')
# ax.plot(mya[0], mya[1]*np.sqrt(2), 'o', label='Data rt2')

lgd = ax.legend(bbox_to_anchor=(1.35, 1), prop={'size':20})
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlabel(r'$R_T$ ($\Omega$)', fontsize=20)
ax.set_ylabel(r'RMS Demod Noise (counts)', fontsize=20)
ax.set_title(r'TRead_Standard, G = {0}, I = {1} $\mu$A'.format(int(gain_unique[j]),
                                                               int(np.round(G_dacs[i]*513))), fontsize=24)

# ax.set_ylim(ymin=np.min(dV_Gs_tot)*vfact/10.)
ax.set_ylim(ymin=1.e-0)
ax.tick_params(labelsize=16)
fig.savefig('noise_model_vs_data.png', dpi=150, bbox_extra_artists=(lgd,), bbox_inches='tight')



# Plot significant components vs R_T
threshhold = 0.5  # Plot threshhold for individual components, i.e. if ever comp/tot > threshhold, plot it
print "1st Index (ADAC) = ", adac_unique
print "2nd Index (Gain) = ", gain_unique
i = 2
# j = 0
print "Using: ADAC = {0}, Gain = {1}".format(adac_unique[i], gain_unique[j])
print "Excitation I = {0} nA".format(G_dacs[i]*513.)

for i in range(5):
    fig, ax = plt.subplots(figsize=(12, 9), dpi=150)
    colors = ['b', 'g', 'r', 'c', 'm']
    for j in range(5):
    # ddgs = dDemod_Gs[i][j]
        ddgt = dDemod_Gs_tot[i][j]
        # ddgc = dDemod_Gs_components[i][j]
        # vfact = np.sqrt(1.0736*920.)
        vfact = np.sqrt(f_filt)  # Use FIR equivalent bandwidth
        c = colors[j] + '--'  # dashed line of i-th color
        ax.plot(R_Ts, ddgt*vfact, c, label='G = {0}'.format(gain_unique[j]), linewidth=2)
        # for k in range(len(ddgs[0].names)):
        #     name = ddgs[0].names[k]
        #     value = ddgc[k]
        #     if np.any(value/ddgt > threshhold):
        #         ax.plot(R_Ts, ddgt*0 + value*vfact, label=name, linewidth=2)

        mya = sdata_ag[i, j, :, (1, 5, 9)]  # 1st -> adac, 2nd -> gain, 3rd -> row, 4th -> column
        # col 1 = expected resistance, col 5 = Demod (std)
#         ax.plot(mya[0], mya[1], 'o', label='Data')
#         ax.plot(mya[0], mya[1]*np.sqrt(2), 'o', label='Data rt2')

        # ERROR BARS. Uncomment if want error bars on. Not sure if value is correct...
#         _, caps, _ = ax.errorbar(mya[0], mya[1], yerr=mya[1]/np.sqrt(2*(mya[2] - 1)), fmt='o', label='Data',
#                                  capsize=0)
#         for cap in caps:
#             cap.set_markeredgewidth(2)
        c = colors[j] + 'o'
        ax.plot(mya[0], mya[1], c, label='Data')


    lgd = ax.legend(bbox_to_anchor=(1.27, 1), prop={'size':20})
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_xlabel(r'$R_T$ ($\Omega$)', fontsize=20)
    ax.set_ylabel(r'RMS Demod Noise (counts)', fontsize=20)
    ax.set_title('TRead_Standard', fontsize=24)
    # ax.set_ylim(ymin=np.min(dV_Gs_tot)*vfact/10.)
    ax.set_ylim(ymin=5.e0, ymax=2.e2)
    ax.tick_params(labelsize=16)
    ival = int(np.round(G_dacs[i]*513))
    ax.set_title(r'ADAC = {0} ($I$ = {1} $\mu$A)'.format(adac_unique[i], ival), fontsize=20)
    fname = 'noise_spread_I{0}uA'.format(ival)
    fig.savefig(fname, bbox_extra_artists=(lgd,), bbox_inches='tight')
