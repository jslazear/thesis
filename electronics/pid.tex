\chapter{Continuous-time PID Control Loops}
\label{cha:pid}
\chaptermark{PID Control Loops}

Consider the control system in Fig.~\ref{fig:pid_loop}, which describes a
simple PID controller. The transfer functions of the PID controller and
process are given by $C(s)$ and $P(s)$ in Laplace space. We want to analyze
how well the controller can actuate the system so its output $y$ matches the
reference $r$, i.e. determine the full system transfer function $H(s) =
Y(s)/R(s)$.

\begin{figure}
\begin{center}
\subimport{imgs/}{pid_loop}
\end{center}
\caption{The control loop block diagram of a simple PID loop. The reference
         (setpoint) signal $r(t)$ is compared against the measured output of
         the process $y(t)$ to form the error $e(t)$. The error $e(t)$ serves
         as the input to the PID controller $C(s)$ to form the process input
         $u(t)$. This input drives the process $P(s)$. Additive noise $n(t)$
         is added to the output of the process to simulate measurement noise
         and forms the measured process output $(y)$. The measured output is
         fed back to the reference signal $r(t)$ to close the loop.}
\label{fig:pid_loop}
\end{figure}

The open loop transfer function $L(s)$ is determined by the response of the
system in the absence of feedback,

\begin{equation}
L(s) = C(s) P(s).
\end{equation}

Upon closing the feedback loop we find that the output depends on the error,
$Y(s) = E(s) L(s) + N(s)$. Combining this with the definition of the error
$E(s) = R(s) - Y(s)$, we find that

\begin{align}
	Y(s) & = \frac{L(s)}{1 + L(s)} R(s) + \frac{1}{1 + L(s)} N(s) \\
	Y(s) & = H(s) R(s) + H_N(s) N(s).
\end{align}

Since there are two inputs (the reference signal $r$ and the noise $n$), we
have a pair of transfer functions. The system transfer function is

\begin{equation}
	H(s) = \frac{Y(s)}{R(s)} = \frac{L(s)}{1 + L(s)} = \frac{C(s) P(s)}{1 + C(s) P(s)} \label{eq:closed_loop_transfer}
\end{equation}

The noise figures into the control system differently and has a noise transfer
function

\begin{equation}
	H_N(s) = \frac{1}{1 + L(s)} = \frac{1}{1 + C(s) P(s)}
\end{equation}

These expressions are generically true for any controller $C(s)$ and process
$P(s)$. The PID controller's transfer function is given by

\begin{equation}
	C(s) = k_P + \frac{k_I}{s} + k_D s
\end{equation}

where $k_P$, $k_I$, and $k_D$ are the proportional, integral, and differential
coefficients.

Consider a process whose response $x(t)$ to an input signal $u(t)$ described
by the first order differential equation

\begin{equation}
	\D{x(t)}{t} + a x(t) = b u(t)
\end{equation}

with transfer function

\begin{equation}
	P(s) = \frac{X(s)}{U(s)} = \frac{b}{s + a}.
\end{equation}

This describes a generic first order process.

A second order process\footnote{Note this is not fully generic. The fully
generic form additionally allows a driving term proportional to
$\D{u(t)}{t}$.} and its transfer function are given by

\begin{subequations}
\begin{gather}
	\D[2]{x(t)}{t} + a_1 \D{x(t)}{t} + a_0 x(t) = b u(t) \\
	P(s) = \frac{b}{s^2 + a_1 s + a_0}.
\end{gather}
\end{subequations}

Since second order processes describe harmonic oscillators, they are
frequently recast in the standard notation

\begin{subequations}
\begin{gather}
	\D[2]{x(t)}{t} + 2\zeta \omega_0 \D{x(t)}{t} + \omega_0^2 x(t) = K \omega_0^2 u(t) \\
	P_0(s) \equiv P(s) = K \frac{\omega_0^2}{s^2 + 2\zeta \omega_0 s + \omega_0^2}
\end{gather}
\end{subequations}

where $\omega_0$ is the natural frequency, $\zeta$ is the dissipation
constant, and $K$ is the gain. Our basic strategy for understanding most
control systems will be to match their transfer functions to a 2nd order
system and then use the well-known solutions of the harmonic oscillator
equations to compute the response of the control system.

The frequency space transfer function and phase of the harmonic oscillator
system are

\begin{subequations}
\begin{gather}
	\Abs{P_0(\omega)} = \Abs{P(\omega)} = \frac{K \omega_0^2}{\sqrt{\left( \omega^2 - \omega_0^2 \right)^2 + \left( 2\zeta \omega_0 \omega \right)^2}} \\
	\arg P_0(\omega) = \arg P(\omega) = \arctan \left( \frac{2 \zeta \omega_0 \omega}{\omega^2 - \omega_0^2} \right)
\end{gather}
\end{subequations}

and the transient (homogeneous) solutions are

\begin{equation}
	x_0(t) \equiv x(t) =
	\begin{cases}
	    e^{-\zeta \omega_0 t} \left( c_1 e^{i \bar{\omega} t} + c_2 e^{-i \bar{\omega} t} \right) & \zeta \neq 1 \text{ (underdamped/overdamped)} \\
	    \left( c_1 + c_2 t \right) e^{-\omega_0 t} & \zeta = 1 \text{ (critically damped)}
	\end{cases}
\end{equation}

where $\bar{\omega} = \omega_0 \sqrt{1 - \zeta^2}$. We note for $\zeta < 1$,
$\bar{\omega}$ is real and the response is oscillatory, corresponding to the
underdamped case. For $\zeta > 1$, $\bar{\omega}$ is imaginary and the
response is purely exponential, corresponding to the overdamped case.

Next considering the fully generic second order system

\begin{equation}
	P(s) = K \frac{\beta \omega_0 s + \omega_0^2}{s^2 + 2\zeta \omega_0 s + \omega_0^2} = \frac{\beta s}{\omega_0} \frac{K \omega_0^2}{s^2 + 2\zeta \omega_0 s + \omega_0^2} + \frac{K \omega_0^2}{s^2 + 2\zeta \omega_0 s + \omega_0^2} \label{eq:second_order_system}
\end{equation}

we observe that the frequency response and phase are

\begin{subequations}
\begin{gather}
	\Abs{P(\omega)} = \Abs{P_0(\omega)} \sqrt{1 + \beta^2 \omega^2} \\
	\arg P(\omega) = \left( \frac{\omega}{\omega_0} \right) \left( \frac{2\zeta \omega_0^2 + \beta\left( \omega^2 - \omega_0^2 \right)}{2\beta \zeta \omega_0^2 + \left( \omega^2 - \omega_0^2 \right)} \right)
\end{gather}
\end{subequations}

and the transient response is

\begin{equation}
	x(t) = x_0(t) + \frac{\beta}{\omega_0} \D{x_0(t)}{t}
\end{equation}

It is usually desireable for $\beta$ to be small so that the response is close
to the ideal sinusoid response.

\section{State-space Representation of Systems}
\label{sec:state_space_representation_of_systems}

For complicated systems it may be more convenient to work with the systems in
state-space. The state-space representation utilizes a set of intermediate
variables such that the output is linear with respect to the state variables
and the input, and such that the state variable evolution is a first order
equation of the state variables and input. Using $y$ for the output, $x$ for
the state variables, and $u$ for the input, we have

\begin{subequations}
\begin{align}
	\D{x}{t} & = A x + B u \\
	y & = C x + D u.
\end{align}
\end{subequations}

Note that $x$, $y$, and $u$ may be vectors and $A$, $B$, $C$, and $D$ are
matrices. The transfer function is straight-forward to compute by Laplace
transforming the system directly,

\begin{align*}
	s X(s) & = A X(s) + B U(s) \\
	Y(s) & = C X(s) + D U(s)
\end{align*}

and eliminating $X(s)$,

\begin{equation}
	H(s) = \frac{Y(s)}{U(s)} = C \left( sI - A \right)^{-1} B + D
\end{equation}

\section{First Order Systems}
\label{sec:first_order_systems}

We may now expand the transfer function of the control system's transfer
function using the expressions for $C(s)$ and $P(s)$. We first consider a
generic first order process.

\begin{equation*}
	H(s) = \frac{b\left( k_D s^2 + k_P s + k_I \right)}{(1 + b k_D)s^2 + (a + b k_P)s + b k_I}
\end{equation*}

We observe that the differential term overcontrols the system and is
unnecessary. With $k_D = 0$, we find

\begin{equation}
	H(s) = \frac{b \left( k_P s + k_I \right)}{s^2 + (a + b k_P)s + b k_I}
\end{equation}

which is a second order system. The DC response $H(0) = 1$ as long as $k_I
\neq 0$, reflecting the well-known fact that a pure proportional controller
has an offset in the response, but an integral controller does not. If $k_I =
0$, then  $H(0) = 1$ iff $a = 0$, which is a very uninteresting process
system.

This system may be compared against the harmonic oscillator system
Eq.~\eqref{eq:second_order_system} to get the correspondence

\begin{subequations}
\begin{gather}
	b k_I = \omega_0^2 \\
	a + b k_P = 2 \zeta \omega_0 \\
	b k_P = \beta \omega_0
\end{gather}
\end{subequations}

\section{Second Order Systems}
\label{sec:second_order_systems}

Next we compute the transfer function of a generic second order process.

\begin{equation}
	H(s) = \frac{\left( \frac{b_1 k_D}{1 + b_1 k_D}\right) s^3 + \left( \frac{b_1 k_P + b_2 k_D}{1 + b_1 k_D} \right) s^2 + \left( \frac{b_1 k_I + b_2 k_P}{1 + b_1 k_D} \right) s + \frac{b_2 k_I}{1 + b_1 k_D} }{s^3 + \left( \frac{a_1 + b_1 k_P + b_2 k_D}{1 + b_1 k_D} \right) s^3 + \left( \frac{a_1 + b_1 k_P + b_2 k_D}{1 + b_1 k_D} \right) s^2 + \left( \frac{a_2 + b_1 k_I + b_2 k_P}{1 + b_1 k_D} \right) s + \frac{b_2 k_I}{1 + b_1 k_D}}
\end{equation}

This is typically compared against a harmonic oscillator system with an extra
pole, for which the homogeneous system is $(s + \alpha \omega_0)(s^2 +
2\zeta \omega_0 s + \omega_0^2)$. This gives the correspondence

\begin{subequations}
\begin{gather}
	\frac{a_1 + b_1 k_P + b_2 k_D}{1 + b_1 k_D} = (\alpha + 2\zeta) \omega_0\\
	\frac{a_2 + b_1 k_I + b_2 k_P}{1 + b_1 k_D} = (1 + 2\alpha \zeta) \omega_0^2\\
	\frac{b_2 k_I}{1 + b_1 k_D} = \alpha \omega_0^3
\end{gather}
\end{subequations}
