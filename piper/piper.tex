% PIPER Science Goals and Design
% [contents only, ordering TBD]
% - low ell (large angular scale) - full sky survey, wide beam, low pointing requirements
% - background limited sensitivity (sensitivity only miproved by more photons)
% - -> open aperture bucket dewar/all cold optics, large cold detector arrays
% - -> MCE and fast SQUID readout
% - -> (and) cryogenics
% - low background -> high altitude balloon
% - foreground control -> 4 frequency bands
% - no cross-linking error, simplified scan strategy (simpler instrument design) -> instantaneous IQUV -> twin polarization-sensitive telescopes
% - systematic control -> fast front-end polarization modulation
% - no Q-U leakage -> VPMs -> Q-V and U-V leakage, but V ~ 0
% - no clock-induced sidelobes, no interpolation error, no uncontrollable cross-talk -> fully synchronous, sensibly-grounded electronics
% - minimally complex instrument -> conventional balloon flight, single set of broadband detectors, single set of reflective optics, single set of VPMs, no downlink, no solar panels, autonomous operation
% - optics summary (refer to Joseph's work)

\chapter{PIPER Science Goals and Design}
\label{sec:piper}
\chaptermark{PIPER Science Goals and Design}

The primary aim of PIPER is to constrain Inflation. We have seen in
Sec.~\ref{sec:quantifying_inhomogeneities} that characterizing the
tensor-to-scalar ratio $r$ will provide us information on the inflaton
potential $V(\phi)$. The scalar spectrum has already been measured, so we
target the tensor spectrum, observationally encoded in the
$C_\ell^\mathrm{BB}$ power spectrum (Sec.~\ref{sub:the_b_mode_spectrum}). We
have discussed in Sec.~\ref{sub:the_b_mode_spectrum} a few aspects of the
B-mode spectrum that make measuring the primordial B-mode spectrum, and thus
$r$, challenging. In this chapter, we discuss how the PIPER instrument
addresses these challenges. A schematic diagram of the PIPER instrument is
shown in Fig.~\ref{fig:piper_block_diagram}.

\begin{figure}[ht]
\begin{center}
    \includegraphics[width=\textwidth]{imgs/piper_block_diagram}
\end{center}
\caption{The PIPER telescope design. One of the twin co-pointed telescopes is
        depicted. The optical design is simplified in this figure (see
        Fig.~\ref{fig:optics} for the full design). The entire telescope is
        enclosed in an open-aperture bucket dewar filled with Liquid Helium at
        1.5 K. The first element is the VPM, responsible for modulating the
        polarization. A vacuum vessel in the dewar houses the analyzer grid,
        bandpass filters, and detectors. The ADR and SQUID amplifier are also
        housed in the pressure vessel. The ADR cools the 4 detector arrays to
        100 mK. The detectors, SQUID amplifier, and ADRs are magnetically
        shielded. The SQUID amplifier exits the dewar via a vacuum-sealed
        trunk directly into the MCE. The ADRs are also controlled by the warm
        housekeeping electronics (HKE), which are also responsible for
        measuring the VPM phase and pointing sensors. Data is stored on a
        flight computer. A communications link is provided by the CIP.
        Dot-dashed lines indicate fiber optic connections.}
\label{fig:piper_block_diagram}
\end{figure}

\FloatBarrier

\section{Sky Coverage}
\label{sub:sky_coverage}

From Fig.~\ref{fig:BB_limits}, we see that the dominant features in the
Inflationary B-mode spectrum are the Reionization bump at $\ell < 10$ and the
Recombination peak
%
\footnote{This feature is called a ``peak'' because the $D_\ell^{BB}$ is
frequently plotted instead of $C_\ell^{BB}$, in which the $\ell \sim 100$
feature appears peaked.}
%
near $\ell \sim 100$. The Reionization bump is larger in amplitude and is not
contaminated by lensing modes, so PIPER targets larger angular scales. Larger
scales require large sky coverage. From Eq.~\eqref{eq:Chatell}, we see that
our ability to constrain the spectrum depends on the number of modes available
to us. For multipole $\ell$, the characteristic scale of a mode is $\theta
\sim \pi/\ell$. Then if we observe a fraction $f_s$ of the sky, we will get
roughly
%
\begin{equation}
	N \sim \frac{4\pi f_s}{\pi \theta^2 / 4} = \frac{16 f_s}{\theta^2} \simeq 1.62 f_s \ell^2
\end{equation}
%
modes from a particular multipole $\ell$. For low $\ell$, the number of
available modes on the sky is small, so the marginal improvement in
uncertainty with sky fraction $\D{}{f_s} \left(
\frac{1}{\sqrt{N}} \right) \propto -1/\ell f_s^{3/2}$ is large.

To measure as many modes as possible, PIPER will map about 90\% of the sky.
For $r \sim 0.01$, the low multipoles will be cosmic variance limited. The
higher multipole limit is set by the beam size. Features comparable in scale
to and smaller than the beam are smeared out by the beam window function and
cannot be resolved. The beam size is chosen to be $\sim 20$ arcmin so that
PIPER is able to resolve multipoles safely above the Recombination peak near
$\ell \sim 100$. With these choices, PIPER covers the range of multipoles
between 2 and 200, covering both the Reionization bump and Recombination peak.
We note, however, that PIPER has limited capability to constrain and eliminate
lensing modes that will contaminate the Recombination peak. In order to get a
handle on lensing modes, PIPER would have to decrease the beam size
considerably, which would make the experiment significantly more complicated.
Rather, PIPER will rely on other experiments\cite{niemack_actpol:_2010,
van_engelen_atacama_2015, austermann_sptpol:_2012,
polarbear_collaboration_measurement_2013} with smaller beams and sensitivity
to large $\ell$ to model the lensing potential. The lensing potential combined
with the well-known E-mode spectrum will allow the B-mode spectrum to be
cleaned of lensing modes.

\section{Frequency Bands}
\label{sub:frequency_bands}

Foregrounds are brighter than the primordial B-mode signal at all scales and
at all frequencies (Sec.~\ref{sub:the_b_mode_spectrum}), so the foregrounds
must be corrected for. PIPER will fly 4 frequency bands at 200, 270, 350, and
600 GHz, all above the CMB peak frequency (160 GHz). All of these frequency
bands are in a regime where the polarized dust emission foreground is
dominant. The 200 and 270 GHz bands have significant CMB contributions and
constitute the science bands. The 350 and 600 GHz bands are almost entirely
dust-dominated and provide maps that allow the dust model to be constrained.
As an added bonus, the dust maps are interesting in and of themselves as they
provide information on interstellar dust.

Interstellar dust does not have rotational invariance. The correlation length
is small, so each pixel is approximately independent. This means that each
frequency band allows only a single model parameter to be constrained when
using only internal data. With two high-frequency bands, PIPER will be able to
remove more complicated dust models.

Planck has recently made nearly full-sky polarized thermal dust emission maps
using its frequency bands at 353 GHz and higher, combined with external data
sources. Planck uses a simplified single-temperature modified blackbody for
its thermal emission model. In the range of frequencies between 353 GHz to
3000 GHz, the data is well-matched to this model with variation in the
modified blackbody temperature of $\sigma_T = \unit[1.4]{K}$ and spectral
index $\sigma_\beta = 0.1$. However, at frequencies below 353 GHz, the
spectral index flattens out considerably and has a larger variation across the
sky\cite{planck_collaboration_planck_2014-2}. Furthermore, the Planck dust
foreground maps do not have the power to distinguish between various dust
models\cite{planck_collaboration_planck_2015-2}. PIPER maps will be made with
greater sensitivity that allow PIPER to discriminate between dust models.

\section{Environment and Scan Strategy}
\label{sub:environment}

PIPER will operate above most of the atmosphere at 120,000 feet (36 km) on a
high-altitude balloon in order to minimize atmospheric foregrounds. At these
altitudes, the atmosphere is only minimally polarized, so the advantage is to
the total loading on the detectors. With reduced loading, the detectors may be
more weakly coupled to their thermal bath and their thermal bath may be at a
lower temperature, both of which reduce the intrinsic noise of the detectors.

PIPER will fly on conventional high-altitude balloon flights with the support
of the Columbia Scientific Ballooning Facility (CSBF) out of Fort Sumner, NM
and Alice Springs, Australia. Each flight allows for about 30 hours of flight
time, with a launch at dawn providing 2 days and 1 night of integration time.
PIPER will use a constant-elevation azimuthal spin at $\unit[\sim 0.5]{deg/s}$
during the night and constant-elevation anti-solar scans for the daytime,
allowing a sky coverage of about 55\% per flight in the hemisphere of the
launch site. A flight out of each of the northern and southern hemispheres
would allow PIPER to map about 90\% of the full sky.

Each flight will have sensitivity to a single frequency band, so a total of
8 flights will be required to get (nearly) full-sky maps in all 4 frequency
bands.

\section{Dewar and Optics}
\label{sub:dewar_and_optics}

PIPER will use the ARCADE2\cite{singal_arcade_2011} 3500 L open-aperture
liquid Helium bucket dewar to house the telescopes. An advantage of being in
the upper atmosphere is the greatly reduced efficacy of the thermal transport
mechanisms. With only about 0.5\% the atmosphere, the conduction and
convection mechanisms are weak enough that the LHe bath at launch will last
throughout the entire 30-hour flight%
\footnote{A quick estimate of the LHe hold time. The latent heat of
evaporation of LHe at 1.4 K is $L(\unit[1.4]{K}) \sim \unit[90]{J/mol} =
\unit[3375]{J/L}$. Supposing about half of the LHe boils off getting to float
(through increased thermal loading at low altitudes and the power necessary to
cool the LHe bath), we are left with about 2000 L of LHe. This has a heat
capacity of about 6 MJ. The loading at float is dominated by the wall
conduction and is about 40 W, which gives us a hold time of 40 hours.}
%
Although the atmospheric pressure is small, the temperature at float altitude
is still about 240 K, so an optical window would be a significant emission
source in the sub-mm range. With the open bucket dewar, the window may be
eliminated.

With the absence of a window, all of the optical elements in the PIPER
telescopes may be held at 1.5 K or colder. At these temperatures, they do not
thermally emit significant amounts of power, allowing the power loading on the
detectors to be further reduced. The optical design is described in detail by
Eimer\cite{eimer_primordial_2010} and is shown in Fig.~\ref{fig:optics}. All
optical elements outside of the vacuum vessel are kept at 1.5 K by superfluid
LHe pumps.

\begin{figure}[ht]
\begin{center}
    \includegraphics{imgs/optics}
\end{center}
\caption{The PIPER optical design, from Eimer
         2010.\cite{eimer_primordial_2010}}
\label{fig:optics}
\end{figure}

The monocrystalline silicon lenses use a metamaterial anti-reflective
coating\cite{datta_large-aperture_2013}, formed by cutting sub-wavelength
grooves into the surface of the silicon. This has the advantage that the AR
coating and lens are the same material, so there is no thermal strain
associated with the coefficient of thermal expansion (CTE) mismatch between
two different materials. Structures suitable for a broadband AR coating for
200 and 270 GHz may be cut into a single lens, but separate lenses with
individual AR coatings must be made for 350 GHz and 600 GHz. Between flights,
the lenses will be swapped out to match the frequency band of the next flight.

\FloatBarrier

\section{Front-end Polarization Modulation}
\label{sub:front_end_polarization_modulation}

The first optical element is a variable-delay polarization
modulator\cite{chuss_properties_2012, chuss_variable-delay_2014} (VPM), which
consists of a movable flat mirror behind a polarizing free-standing wire
grating. The VPM rotates the Stokes vector between linear ($U$) and circular
($V$) polarization as the mirror-grating spacing is changed. By modulating and
measuring the mirror-grating spacing, the modulation function may be known
precisely. PIPER modulates the polarization signal rapidly at 3 Hz, far faster
than the characteristic frequency of the signal. A multipole $\ell$ has a
characteristic scale $\theta \sim \pi/\ell$ and will appear in the timestream
as a signal at
%
\begin{equation}
	f_\ell \sim \frac{\Omega}{\theta} = \frac{\Omega \ell}{\pi} = (\unit[0.003]{Hz}) \ell
\end{equation}
%
where $\Omega \sim \unit[0.5]{deg/s}$ is the scan rate. For the $\ell \sim
200$ upper limit of PIPER's multipole range, we get $f_\ell \sim
\unit[0.5]{Hz}$. The signal is carried on the 3 Hz VPM carrier modulation
frequency, so the relevant frequencies will be at $\unit[3]{Hz} \pm f_\ell$,
i.e. between 2.5 Hz and 3.5 Hz.

This technique has the advantage that only the sky signal is modulated, so any
instrumental polarization sources will be heavily suppressed by the
demodulation step. Essentially, the PIPER VPM is an optical lock-in amplifier
with unity gain. The noise properties of the instrument are then dependent
primarily on the local noise spectrum around the carrier frequency of 3 Hz.
Particularly, the 1/$f$ noise below $\sim \unit[1]{Hz}$ is rejected following
post-flight software demodulation. This technique has been demonstrated by the
ABS instrument.\cite{kusaka_modulation_2014} Since the local noise properties
around 3 Hz are the dominant contributors to the total noise, PIPER ensures
that noise sources in the 1-10 Hz range are minimized.

An additional advantage is due to the rotation between linear and circular
polarization. The total sky circular polarization is expected to be
negligible\cite{mainini_improved_2013}, so the VPM modulates with a null
signal\footnote{If the V signal is not null then a measurement of it would be
a significant discovery in its own right.}. We contrast this with the more
conventional strategy of using a waveplate, which rotates between linear ($Q$
and $U$) polarizations. In the waveplate case, errors or uncertainty in the
$Q$-$U$ rotation can cause $E$-$B$ mixing since the rotation is within the
plane formed by the $E$-$B$ basis. The contamination of $E$ into $B$ is
problematic since the $B$ signal is expected to be a factor of $r \sim 0.01$
smaller than the $E$ signal. Even a 1\% contamination could entirely swamp the
$B$ signal. This is not an issue for the VPM since the rotation is in a plane
orthogonal to the $E$-$B$ plane.

\section{Twin Co-pointed Telescopes}
\label{sub:twin_co_pointed_telescopes}

A single telescope with a VPM modulates between $U$ and $V$ and therefore only
allows precise measurement of $U$ and $V$. A full description of the Stokes
vector requires knowledge of $Q$, $U$, and $V$. In order to provide the
missing component, PIPER uses twin co-pointed telescopes with VPM gratings
rotated relative to each other by 45 degrees. The VPM always modulates the
local $U$ and $V$, so by rotating the VPM grating allows one telescope to be
sensitive to the sky $U$ and sky $V$ while allowing the other telescope to be
sensitive to sky $Q$ and sky $V$. We note that the gratings are rotated by 45
degrees rather than 90 degrees because of the spin-2 property of the $Q$-$U$
basis (Sec.~\ref{sub:polarization_mechanisms}). The instrument optics has
mirror symmetry about the saggital plane with completely independent optical
paths for each telescope.

The twin telescope and rapid polarization modulation have a significant
advantage in that they allow PIPER to have an ``instantaneous'' measurement of
the full Stokes vector, in the sense that a complete measurement is made
roughly every VPM swing, i.e. at 3 Hz. PIPER does not rely on revisiting a
spot in the sky with some other instrument configuration (e.g. rotated
boresight or different waveplate angle). Cross-linking is only required for
instrument calibration, which may be done with a small region since it has
comparably few free parameters. The instantaneous measurement mitigates the
effect of any slow instrumental drifts and also allows PIPER to use its
extremely simple scan strategy.

\section{Detectors and Sensitivity}
\label{sub:detectors}

PIPER will use 4 arrays, each with a 32 $\times$ 40 grid of Transition-Edge
Sensor (TES) bolometers\cite{irwin_transition-edge_2005}. Each pixel comprises
an absorber strongly thermally coupled to a superconducting thermistor (the
TES) with transition temperature $T_c = \unit[140]{mK}$. The absorber and
thermistor are weakly thermally coupled ($G = \unit[30]{pW/K}$) to a 100 mK
thermal bath. A voltage bias is applied to the TES to hold it on the
superconducting transition. On the transition, the temperature sensitivity
$\left( \D{R}{T} \right)$ is enormous, allowing for excellent sensitivity to
applied optical power. The negative electrothermal feedback (ETF) of the
voltage-biased positive temperature coefficient thermistor expands the dynamic
range of the detector. The details of the detectors are discussed in
Sec.~\ref{sec:tes_physics}.

Phonon (thermal) noise is intrinsic to all detectors and depends on the
strength $G$ of the thermal link and temperature of the detectors,
$P_\mathrm{phonon} \propto \sqrt{G T}$. We have seen in the previous sections
that the design of PIPER allows for a smaller $G$, which in turn reduces the
phonon noise. The other intrinsic noise source is the inherent variability of
the signal from the sky, the photon noise, which we emphasize is a property of
the signal and not the instrument. At $T = \unit[140]{mK}$ and $G =
\unit[30]{pW/K}$, the phonon noise is smaller than the photon noise, so the
detectors are so-called ``background-limited''\cite{richards_bolometers_1994}.
Other sources of noise are eliminated by the polarization modulation discussed
in the previous section. In the background-limited regime, the total
instrument sensitivity is determined only by the number of photons collected
from any given region of the sky. As such, further improvements may only be
made by collecting more photons: increasing the size of the primary, improving
the optical efficiency, or increasing the integration time. PIPER achieves an
instrument instantaneous sensitivity of $\unit[1.3]{\mu K\,\sqrt{s}}$ at 200
GHz and $\unit[1.6]{\mu K\,\sqrt{s}}$ at 270 GHz.

The detectors are inherently broadband with a reflective backshort that
increases the absorptivity at 200, 270, and 350 GHz and attenuates it at 600
GHz. The attenuation at 600 GHz is required to prevent overwhelming the small
saturation power $P_\mathrm{sat} = \unit[1.2]{pW}$ by the increased power of
atmospheric emission at higher frequencies. Since the detectors are broadband,
the frequency sensitivity of the telescope is determined by a micromesh
bandpass filter\cite{ade_review_2006} at the opening of the detector package.
The filter must be swapped out between flights. Furthermore, the detectors are
sensitive only to the total intensity of light incident upon them, so the
polarization sensitivity is provided by the combination of the VPM and the
analyzer grid.

\section{Cryogenics}
\label{sub:cryogenics}

An adiabatic demagnetization refrigerator (ADR)\cite{shirron_multi-stage_2000,
shirron_development_2004} is used to cool the detectors from the LHe bath
temperature to the 100 mK base temperature. An ADR is a heat pump that
exchanges the entropy of a paramagnetic salt for heat using the magnetic field
from a superconducting coil. The 4-stage ADR allows for the coldest stage to
hold at 100 mK continuously while rejecting up to $\unit[10]{\mu W}$ of power
from both a 1.4 K LHe bath as expected at float and a 4.2 K LHe bath as
expected on the ground.

\section{Detector Readout}
\label{sub:detector_readout}

The detectors are read out by a 2-stage Superconducting Quantum Interference
Device (SQUID)\cite{clarke_squid_2004} pre-amplifier and the Multi-Channel
Electronics (MCEs)\cite{battistelli_functional_2008} (discussed in detail in
Chapter~\ref{sec:sptd}). The SQUID amplifier's first stage also serves as the
detector array's multiplexer and is built into the backside of the detector
array. The SQUID pre-amplifier is cryogenic and low-noise to ensure the total
measurement noise is dominated by the photon noise. The warm MCEs use a
time-domain multiplexing scheme in which the 40 rows are read out in series,
while all 32 columns are read out simultaneously in parallel. Each of the 4
detector arrays requires its own set of MCEs.

The first stage SQUID amplifier uses a nulling feedback loop to ensure
stability. The MCEs provide a row revisit rate of about 12 kHz, which is used
to drive the first stage feedback loop. A final digital filter reduces the
signal bandwidth to about 30 Hz, significantly more than the 3 Hz modulation
carrier wave generated by the VPM.

All four MCE racks are driven synchronously by a common clock to ensure that
data collection is perfectly synchronous. The MCE racks are powered entirely
by batteries.

\section{Electronics}
\label{sub:electronics}

The detector data does not constitute the full set of science data for the
instrument. In addition to the power at the detector, we must also know the
VPM's grating-mirror spacing to construct the demodulation function, and the
telescope pointing to correctly place the detector signal on the sky. PIPER
uses a set of custom low-noise synchronous electronics to perform these tasks.

The PIPER electronics (aka Housekeeping Electronics, or HKE) takes in the MCE
clock as its sole clock and so is by construction synchronized up to a
constant phase with the detector readout. The VPM phase is measured by a
series of capacitive absolute displacement sensors and read out by an HKE fast
ADC board. The pointing is updated by gyroscopes, which are also read out by
an HKE fast ADC board. This allows the 3 science data streams to be reconciled
without a complicated interpolation step. The PIPER pointing actuation uses
synchronous low-noise linear motor controllers. Commercial motor controllers
that are both synchronous to an external clock and linear are not available.
Additionally, since there is only a single clock, no contamination at beat
frequencies is possible. This ensures that contaminants do not pop up at
unexpected frequencies.

The PIPER electronics are also powered entirely by batteries. The short
conventional balloon flights allow PIPER to carry enough batteries to power
the entire instrument for the entire flight without being excessively heavy.

The PIPER electronics are discussed in great detail in
Chapter~\ref{sec:electronics}.

% \section{Instrument Simplicity}
% \label{sub:simplicity}

% The various design decisions in PIPER all combine to allow for PIPER to be a
% simpler instrument. The sensitive detectors and low optical power loading
% allows for enough mapping speed to survey the sky in a single conventional
% balloon flight. The short flight time allows for the windowless design and
% additionally facilitates the frequency sensitivity to be spread over many
% flights. Only requiring a single frequency band per flight allows for a
% simpler optical design. The shorter flights allow for most of the cryogenics
% to be as simple as a dewar of liquid helium and allow for the electronics to
% be powered entirely off of batteries.

\section{Sensitivity}
\label{sub:sensitivity}

With this design, PIPER will achieve sufficient sensitivity to detect or
constrain the tensor-to-scalar ratio to $r < 0.007$ at the $2\sigma$ level. A
successful detection would confirm the epoch of Inflation as a physical
reality and give insight into its energy scale. A null detection would reject
all of the simple models of Inflation and raise significant questions about
its feasibility. Furthermore, PIPER will produce the most sensitive polarized
dust maps of its generation of experiments (Fig.~\ref{fig:poldust}).

\begin{figure}[ht]
\begin{center}
    \includegraphics[width=\textwidth]{imgs/poldust-sn-small}
\end{center}
\caption{PIPER will measure the polarized dust emission to a S/N better than
         10 even in regions of low dust intensity and for polarization
         fractions as small as 10\%, as seen in this simulation of the
         polarized dust of the BICEP2 region. Figure from
         Lazear.\cite{lazear_primordial_2014}}
\label{fig:poldust}
\end{figure}

\FloatBarrier
