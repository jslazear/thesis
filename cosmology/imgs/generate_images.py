#!/bin/python

"""
generate_images.py
jlazear
1/22/15

Generates images used in the docs files.

Example:

$ python generate_images.py
"""
__version__ = 20150122
__releasestatus__ = 'beta'


import os
import sys
import argparse

import wget
import numpy as np
import healpy as hp
import matplotlib.pyplot as plt
import scipy.signal
from scipy.integrate import quad, simps


imgpath = os.path.dirname(os.path.abspath(__file__)) + '/'


# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument("--regenerate",
                    help="force the regeneration of all figures",
                    action="store_true")
args = parser.parse_args()
force = bool(args.regenerate)

strtemplate = "Saving {0} to {1}\r\n"
strexists = "File {0} already exists."

# -------------------------------------
# ---------- Generate images ----------
# -------------------------------------

fname = imgpath + 'ClBB_base.eps'
if os.path.isfile(fname) and not force:
    print strexists.format(fname)
else:
    data = np.loadtxt('explanatory01_cl.dat')
    data2 = np.loadtxt('explanatory01_cl_lensed.dat')
    ells, TTs, EEs, BBs, TEs, dds, dTs, dEs = data.T
    ells2, TTs2, EEs2, BBs2, TEs2, dds2, dTs2, dEs2 = data2.T

    BBs3 = BBs[:len(ells2)]
    BBs_lensed = BBs2 - BBs3

    fig, ax = plt.subplots(figsize=(10, 6))
    ax.plot(ells, BBs*2*np.pi/(ells*(ells+1)), 'k', linewidth=1.5)
    ax.plot(ells2, BBs_lensed*2*np.pi/(ells2*(ells2+1)), 'b', linewidth=1.5)
    ax.set_yscale('log')
    ax.set_xlim(1, 2000)
    ax.set_ylim(1.e-8, 1.)
    ax.set_xscale('log')
    locator = plt.LogLocator(numticks=5)
    ax.yaxis.set_major_locator(locator)
    plt.tick_params(axis='both', which='major', labelsize=16)
    formatter = plt.FormatStrFormatter('%.0f')
    #formatter = ax.get_xaxis().get_major_formatter()
    ax.xaxis.set_major_formatter(formatter)
    ax.set_xlabel(r'$\ell$', fontsize=20)
    ax.set_ylabel(r'$C_\ell^{BB} \ \ [\mathrm{\mu K^2}]$', fontsize=20)
    fig.savefig(fname)
