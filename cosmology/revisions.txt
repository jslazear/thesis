===From cbennett===

[DONE]
"Inflation is an epoch of accelerating expansion, a > 0, that is theorized to have taken place shortly after the Big Bang and before the most recent epoch of radiation domination."
Say what your definition of the "Big Bang" is in this statement.

[DONE]
Say which physical quantity is scale-invariant when n_s=1.

[???] That depends on the initial curvature, which is unknown (and likely unknowable).
Say, roughly speaking, at what level would you expect to observe a deviation from perfect flatness?

[DONE]
"The rapid expansion of spacetime associated with Inflation would produce a background of gravitational waves (metric tensor perturbations). Similar to the thermal fluctuations, these perturbations would have been expanded by Inflation and would produce a background of gravitational waves."
Say how a rapid expansion from inflation (driven by a scalar field) generates (tensor) gravitational waves?  Say why are there any gravitational waves at all?

[DONE]
"The pivot scale is usually placed into the subscript of r (e.g. r_0.002 or r_0.05) to eliminate any ambiguity."
State what the units of these numbers are.

[DONE]
"The current upper bound on r is r < 0.113."
I believe it is lower than that.  You might also want mention limits based on CB temperature vs. polarization and that the temperature measurements are CV-limited -- so polarization measurements are no necessary for progress to be made.

[DONE]
Personally, I don't like the use of the term, "anisotropies". Anisotropy means different in different directions. The plural doesn't make sense to me. Anisotropies seems to be a term used to mean fluctuations.  You may leave it as it is since it is very common usage, albeit it improper.  Just saying.

[DONE]
"Clearly the Q, U basis is not rotationally invariant, which makes it inconvenient."
"The Q, U basis is not rotationally invariant, which is not convenient for working with on the surface of a sphere."
This is more than just an inconvenience - it is a clear statement that Q and U cannot be the fundamental variables that convey cosmological physics.  The E-modes and B-modes are coordinate-independent and thus can directly convey the physics.

[???] Figure made by me, based upon similar figures from a number of other papers. Could cite one of them (or my SPIE paper, which included it as a subfigure).
If you took Figure 1.1 from somewhere, it should be cited.

[DONE]
"The B-modes are particularly interesting because scalar perturbations cannot create B-mode fluctuations,4 so a non-zero B-mode signal is indicative of tensor perturbations. The only tensor perturbations in the early universe were the Inflationary gravitational wave background discussed in Sec. 1.2.1, so a detection of primordial B-modes is a conclusive detection of Inflation."
Why is a detection of (tensor) B-modes a conclusive detection of inflation, when the inflaton field is a scalar field and cannot produce B-modes?
You say yourself:
"Thus, we would not expect scalar perturbations to produce B-mode power because the scalar perturbations have too much symmetry."

[DONE]
"The lensing signal is insignificant on large scales,"
I wouldn't say that.  How significant it is depends on the value of r, which we do not know.  I would agree that it is insignificant for the current generation of l<12 experiments (CLASS and PIPER).

[DONE]
Equation 1.55 is for intensity, not polarization.  According to Planck. the polarized spectral index of the dust is 1.59 ± 0.02. That's a pretty small uncertainty but I don't recall over what masked sky that applies.

[DONE]
Fig 1.4: If these are polarized signals plotted then the label axis "brightness temperature" is wrong. It should be rms polarization (in thermodynamic units).  Yes, Planck labeled this incorrectly in their paper too.

===From eswitzer===

[DONE]
The cosmology chapter is very good. You could consider adding a note about transferal of inhomogeneity to anisotropy between eq1.48 and 1.53 -- e.g. the a_lm' in 1.48 are in the local frame of the inhomogeneity, but transfer Q/U polarization to the effective surface of last scattering. http://arxiv.org/pdf/0802.3688v1.pdf has Eq. 27 that could go in the temperature anisotropy section. Rather than get into the complication for polarization, you could refer to that simpler temperature equation as being basically the same transferal concept.

===From akogut===

Miscellaneous edits.

[UNCHANGED] Unclear to me what point is trying to be made. Is there any reason to think that tensor modes have mass?
Section 1.2.1 (Gravitational Waves): "Qualify: Isn’t this statement only true for massless fields?"

[DONE] Citation for this?
Section 1.6.3 (Polarized Foregrounds): "Although in practice superposition reduces this to f < 0.3"